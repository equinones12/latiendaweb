<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Cargos;
use app\models\Rol;
use kartik\date\DatePicker;

use yii\helpers\Url;
use app\models\TiposDetalles;
use app\models\Especialidades;

$this->title = 'Registro de Usuario';
?>
  <!-- <script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script> -->
<?php $form = ActiveForm::begin([
    'method' => 'post',
    'id' => 'formulario',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
]);
?>

  <div class="card ">
    <div class="card-header card-header-primary">
      <h3 class="card-title">Información Personal </h3>
    </div>
    <!-- /.card-header -->
    <!-- card body -->
    <div class="card-body">
      <div class="row">
        <div class="col-lg-6">
        <?= $form->field($model, "nombres") ?>   

        </div>
        <div class="col-lg-6">
        <?= $form->field($model, "apellidos") ?>   

        </div>
        <div class="col-lg-6">
        <?= $form->field($model, "numeroIdentificacion") ?>   

        </div>
        <div class="col-lg-6">
        <?= $form->field($model, "telefono") ?>   

        </div>
        <div class="col-lg-6">
        <?= $form->field($model, "direccion") ?>   

        </div>
      </div>

    </div>            
  </div>    


  <!-- ====== INFORMACION DE LA APLICACION ============ -->
  <!-- general form elements -->
  <div class="card ">
    <div class="card-header card-header-primary">
      <h3 class="card-title">Información del Usuario </h3>
    </div>
    <!-- /.card-header -->
    <!-- card body -->
    <div class="card-body">
      <div class="row">
        <div class="col-lg-6">
          <div class="form-group">
            <?= $form->field($model, "username")->input("text") ?>   
          </div>
        </div>
        <div class="col-lg-6">
          <div class="form-group">
            <?= $form->field($model, "email")->input("email") ?>  
          </div>
        </div>
        <div class="col-lg-6">
          <div class="form-group">
            <?= $form->field($model, "password")->input("password") ?>  
          </div>        
        </div>
        <div class="col-lg-6">
          <div class="form-group">
            <?= $form->field($model, "password_repeat")->input("password") ?>   
          </div>
        </div>
      </div>

      
      
      
      <div class="form-check">
        
      </div>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->

 

  
 
      
        
  <!-- ============== ROLES ASOCIADOS AL PERFIL ============ -->
  
  <div class="card ">
    <div class="card-header card-header-primary">
      <h3 class="card-title">Roles para el Perfil </h3>
    </div>
    <!-- /.card-header -->
    <!-- card body -->
    <div class="card-body">
      <div class="row">
        <div class="col-lg-12">
          <?php foreach ($roles as $r) { ?>
          <button type="button" class="btn btn-primary btn-sm addRole" id="<?php echo $r->id; ?>">
            <span class="fa fa-user" aria-hidden="true"></span> <?php echo $r->nombre; ?>
          </button>
          <?php } ?>
          <?= $form->field($model, "role")->textInput(['readonly'=>'readonly']) ?>   
        </div>
        
        

      </div>

    </div>
  </div>
  

  

  <!-- ============== MENU ASOCIADO AL PERFIL ============ -->
  
  <div class="card ">
    <div class="card-header card-header-primary">
      <h3 class="card-title">Menus para el usuario</h3>
    </div>
    <!-- /.card-header -->
    <!-- card body -->
    <div class="card-body">
      <div class="row">
  
        
          <div class="col-lg-12 " id="demo">
            
            
                  <div class="checkbox pull-right">
                    <label><input  type="checkbox"  checked  id="selectAll" ><strong>Seleccionar todo</strong></label>
                  </div>
              <br>
              <br>
              <?php foreach ($opciones as $o) { ?>
                <div class="checkbox">
                  <label><input class="opcionesCheck"  type="checkbox" name="opciones[]" value="<?php echo $o->id.',0,0' ?>"  id="<?php echo $o->id; ?>" value=""><?php echo $o->nombre; ?></label>
                </div>
                <div style="margin-left: 20px;">
                  <?php foreach ($items as $i) { ?>
                    <?php if ($o->id == $i->id_opcion) { ?>
                      <div class="checkbox">
                        <label><input type="checkbox" name="opciones[]" value="<?php echo $o->id.','.$i->id.',0' ?>" class="itemsCheck" id="<?php echo $i->id; ?>"  data-opcion="<?php echo $o->id; ?>" value=""><?php echo $i->etiqueta; ?></label>
                      </div>

                      <div style="margin-left: 20px;">
                        <?php foreach ($subitems as $s) { ?>
                          <?php if ($i->id == $s->id_item) { ?>
                              <label><input type="checkbox" name="opciones[]" value="<?php echo $o->id.','.$i->id.','.$s->id ?>" class="subitemsCheck"  data-opcion="<?php echo $o->id; ?>" data-item="<?php echo $i->id; ?>"><?php echo $s->etiqueta; ?></label>
                            </div>
                          <?php } ?>
                        <?php } ?>
                      </div>
                    <?php } ?>
                  <?php } ?>
                </div>
              <?php } ?>
            
        </div>
      </div>
    </div>
  </div>




  <div class="form-group col-lg-12">
    <?= Html::submitButton('<i class="fa fa-fw fa-plus"></i> Crear usuario', ["class" => "btn btn-default btn-sm"]) ?>
    <?= Html::a('<i class="fa fa-fw fa-reply"></i> Volver', ['users/index'], ['class' => 'btn btn-primary btn-sm']) ?>
  </div>
    
</div>
<?php $form->end() ?>

<!-- ================ JS =========== -->
<script>
  $(document).ready(function(){
    $(".addRole").click(function(){
      if ($(this).hasClass('select')) {
        $(this).removeClass('select');
        $(this).addClass('btn-primary');
        $(this).removeClass('btn-success');
      }else{
        $(this).addClass('btn-success');
        $(this).addClass('select')
        $(this).removeClass('btn-primary');
      }
      validateRoleActive();
    });
  })
  // =============== FUNCIONES ==================
  function validateRoleActive(){
    var concatRole = '';
    var init = false;
    $(".addRole").each(function(){
      console.log($(this).attr('class'));
      if ($(this).hasClass('select')) {
        if (init) {
          concatRole = concatRole + ',' + $(this).attr('id');
        }else{
          concatRole = $(this).attr('id');
          init = true;
        }
      }
    });
    $("#formregister-role").val(concatRole);
  }
</script>
<script>
  $(document).ready(function(){

    $(".opcionesCheck").click(function(){
      if (!$(this).is(':checked')) {
        $(document).find("[data-opcion='" + $(this).attr('id') + "']").prop('checked', false);   
      }
    });

    $(".itemsCheck").click(function(){
      if (!$(this).is(':checked')) {
        $(document).find("[data-item='" + $(this).attr('id') + "']").prop('checked', false);   
      }else{
        $('#'+$(this).data('opcion')).prop('checked', true);
      }
    }); 

    $(".subitemsCheck").click(function(){
      if ($(this).is(':checked')) {
        $('#'+$(this).data('opcion')).prop('checked', true);
        $('#'+$(this).data('item')).prop('checked', true);
      }
    });

    $("#selectAll").click(function(){
      if ($(this).is(':checked')) {
        $(".opcionesCheck").prop('checked', true);
        $(".itemsCheck").prop('checked', true);
        $(".subitemsCheck").prop('checked', true);
      }else{
        $(".opcionesCheck").prop('checked', false);
        $(".itemsCheck").prop('checked', false);
        $(".subitemsCheck").prop('checked', false);
      }
    });

  })
</script>