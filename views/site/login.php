<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

// $this->title = 'La Salle SG';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login"><br><br><br><br>
    <!-- <h1><?//= Html::encode($this->title) ?></h1> -->
    <div class="col-md-4 col-sm-6 ml-auto mr-auto"><!-- col-lg-4  -->
        <div class="card card-login card-hidden">
            <div class="card-header card-header-primary text-center"  data-background-color="blue">
                <h4 class="card-title">INICIO DE SESIÓN</h4>
                <div class="social-line">
                    
                </div>
            </div>
            <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
//                    
                ]); ?>
            <div class="card-body ">            
                <?= $form->field($model, 'username')->textInput(['placeholder' => 'Usuario', 'class' => 'form-control'])->label(false) ?>      <br>  
                <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Contraseña', 'class' => 'form-control'])->label(false) ?><br>
                <span class="bmd-form-group">
                    <div class="input-group">
                        <?= $form->field($model, 'rememberMe')->checkbox()->label('recordarme aqui') ?><br>
                        
                    </div>
                    
                </span>
            </div>
            <div class="col-lg-12">
            <div class="input-group">
                    <?= Html::submitButton('Ingresar', ['class' => 'btn btn-default btn-block ', 'name' => 'login-button']) ?>    
                    </div>
                
            </div>
           
           <div class="card-footer">
           </div>
           <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php

$this->registerJs("
    $(document).ready(function(){
        
        $('.control-label').css('font-size','12px');
    });");    
?>