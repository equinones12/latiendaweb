<?php 
	use yii\helpers\Html;
	use yii\widgets\DetailView;
	use yii\helpers\Url;
?>

<div class="alert alert-<?php echo $alert ?>" role="alert">
  <span class="fa fa-<?php echo $graphicon;  ?>" aria-hidden="true"></span>
  <?php echo $msg; ?>
</div>
<?php if ($model) { ?>
	<div class="users-view">

	    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
	    <center>
		<div class="jumbotron">
	        <img width="150" src="<?php echo Url::base(); ?>/images/fotos/<?php echo $model->foto; ?>" alt="..." class="img-circle">
	        <p class="lead" style="color:#ffffff"><?php echo $model->nombres.' '.$model->apellidos; ?></p>
	    </div>
		</center>


	    <!-- informacion personal -->
	    <div class="card card-secondary">
	        <div class="card-header">Informacion personal</div>
	        <div class="card-body">
	            <table class="table table-striped">
	                <tbody>
	                  <tr>
	                    <td><strong>Nombres: </strong><?php echo $model->nombres; ?></td>
	                    <td><strong>Apellidos: </strong><?php echo $model->apellidos; ?></td>
	                  </tr>
	                  <tr>
	                    <td><strong>Tipo de estado: </strong><?php echo $model->numeroIdentificacion; ?></td>
	                  </tr>
	                  <tr>
	                    <!-- <td><strong>Direccion: </strong><?php echo $model->direccion; ?></td> -->
	                    <!-- <td><strong>Telefono: </strong><?php echo $model->telefono; ?></td> -->
	                  </tr>
	                  <tr>
	                    <td colspan="2"><strong>Email: </strong><?php echo $model->email; ?></td>
	                  </tr>
	                </tbody>
	              </table>
	        </div>
	    </div>

	    <!-- informacion laboral -->
	    
		<table class="table table-striped">
			<tbody>
				<tr>
				<td><strong>Username: </strong><?php echo $model->username; ?></td>
				<td><strong>Rol usuario: </strong><?php echo $model->rol->nombre; ?></td>
				</tr>
			</tbody>
		</table>
	        
	    <p>
	        <?= Html::a('<i class="fa fa-fw fa-edit"></i> Actualizar información', ['users/update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
	        <?= Html::a('<i class="fa fa-fw fa-plus"></i> Registrar otro funcionario', ['site/register'], ['class' => 'btn btn-primary btn-sm']) ?>
	        <?= Html::a('<i class="fa fa-fw fa-reply"></i> Volver', ['site/index'], ['class' => 'btn btn-primary btn-sm']) ?>
	    </p> 
	</div>

<?php }else{ ?>
	<p>
		<?= Html::a('<i class="fa fa-fw fa-plus"></i> Registrar otro funcionario', ['site/register'], ['class' => 'btn btn-primary btn-sm']) ?>
		<?= Html::a('<i class="fa fa-fw fa-reply"></i> Volver', ['site/index'], ['class' => 'btn btn-primary btn-sm']) ?>	
	</p> 
<?php } ?>