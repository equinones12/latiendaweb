<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
?>
 
<?php $form = ActiveForm::begin([
    'method' => 'post',
    'enableClientValidation' => true,
]);
?>

<div class="card col-lg-12 container-fluid">
    <div class="card-body">

        <h3><?= $msg ?></h3>
        <div class="row justify-content-md-center form-login">
            <div class="col-md-4 col-md-auto contentLogin">

                <center>        
                    <h1>Resetear Contraseña</h1>
                </center> <br>
                <div class="form-group">
                <?= $form->field($model, "email")->input("email") ?>  
                </div>
                
                <div class="form-group">
                <?= $form->field($model, "password")->input("password") ?>  
                </div>
                
                <div class="form-group">
                <?= $form->field($model, "password_repeat")->input("password") ?>  
                </div>

                <div class="form-group">
                <?= $form->field($model, "verification_code")->input("text") ?>  
                </div>

                <div class="form-group">
                <?= $form->field($model, "recover")->input("hidden")->label(false) ?>  
                </div>
                
                <?= Html::submitButton("Reset password", ["class" => "btn btn-primary"]) ?>
                <?php $form->end() ?>
            </div>
        </div>
    </div>
</div>
