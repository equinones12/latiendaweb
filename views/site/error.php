<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;
?>


    
<div class="col-lg-12">

    <div class="jumbotron">
        <img width="150" src="<?php echo Url::base(); ?>/images/error.png" alt="..." class="">
        <br>
        <div class="alert alert-danger">
            <?= nl2br(Html::encode($message)) ?>
        </div>
        <p>
            Por favor contactese con el area de Tecnologia. Gracias
        </p>
    </div>

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    


    

        
</div>
