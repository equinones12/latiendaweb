<?php 
    use yii\helpers\Html;
    use yii\helpers\Url;
    use app\models\Users;
    use yii\bootstrap4\ActiveForm;
    // $this->title = 'Inicio';
?>
  <!-- Content Wrapper. Contains page content -->
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
  <script src="<?php echo Url::base(); ?>/librerias/jquery-3.4.1.min.js"></script>

  <script src="<?php echo Url::base(); ?>/js/highcharts.js"></script>
  <script src="<?php echo Url::base(); ?>/js/exporting.js"></script>
  <script src="<?php echo Url::base(); ?>/js/export-data.js"></script>
  <script src="<?php echo Url::base(); ?>/js/highcharts-3d.js"></script>
  <script src="<?php echo Url::base(); ?>/js/cylinder.js"></script>

<div class="row">
    <div class="col-lg-12 text-center">      
    </div>
    <!-- ./col -->
    <div class="col-lg-3 ">
      <!-- small box -->
      <div class="card card-stats">
        <div class="card-header card-header-primary card-header-icon">
          <div class="card-icon">
            <center>
              <span class="fa fa-calculator" style="width: 80px; "></span>
            </center>
          </div>
          <h5 class="card-title">Peliculas registradas</h5>
          <h3 class="card-category text-danger"><?php echo $peliculas['total']?></h3>
        </div>
        <div class="card-footer">
          <!-- <a href="<?php echo Url::base(); ?>/index.php?r=site%2Falmacenindex" class="small-box-footer">Ir al Módulo <i class="fas fa-arrow-circle-right"></i></a>         -->
          <a class="btn btn-primary btn-block" href="<?php echo Url::base(); ?>/index.php?r=peliculas%2Findex">Ir al Módulo</a>
        </div>
      </div>
    </div>
</div>

<figure class="highcharts-figure">
    <div id="container"></div>
    
</figure>

<div class="col-lg-12">
</div>



<!-- MODAL PARA REASIGNAR LA SUBACTIVIDAD -->


<!--  ========= Js ======== -->
<script>
$(document).on('click','.gestion',function(){
  $("#load").modal({
          backdrop: "static", 
          keyboard: false, 
          show: true 
      });
  });
</script>
