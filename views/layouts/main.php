
<?php 
    use yii\helpers\Html;
    use yii\helpers\Url;
    use app\models\Users;
    use yii\bootstrap4\ActiveForm;

    use yii\bootstrap\Nav;
    use yii\bootstrap\NavBar;
    use yii\widgets\Breadcrumbs;
    use app\assets\AppAsset;
    use kartik\nav\NavX;
    use app\models\OpcionHasRol;
    use app\models\ItemHasRol;
    use app\models\SubitemHasRol;
    use app\models\ControlIngresoUsuarios;
    use app\models\TiposDetalles;
    use app\models\Menuperfil;
    use app\models\OpcionMenu;
    use app\models\ItemMenu;
    use app\models\SubitemMenu;
    use yii\helpers\ArrayHelper;

?>

<html>

<head>
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <?php $this->head() ?>
  <!-- <link rel="stylesheet" href="<?php echo Url::base(); ?>/css/adminlte.css"> -->
  <link href="<?php echo Url::base(); ?>/sbadmin/css/sb-admin-2.css" rel="stylesheet">
  <link href="<?php echo Url::base(); ?>/css/site.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <!-- <link href="<?php echo Url::base(); ?>/sbadmin/css/sb-admin-2.min.css" rel="stylesheet"> -->
  <!-- <link href="<?php echo Url::base(); ?>/sbadmin/css/sb-admin-2.css" rel="stylesheet"> -->
  <!-- <link href="<?php echo Url::base(); ?>/css/site.css" rel="stylesheet"> -->
  <link href="<?php echo Url::base(); ?>/css/style4.css" rel="stylesheet">
  <!-- <link href="<?php echo Url::base(); ?>/css/materialize.css" rel="stylesheet">  -->
  <link href="<?php echo Url::base(); ?>/css/whatsapp.css" rel="stylesheet">

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Software de Control</title>

  <link href="<?php echo Url::base(); ?>/sbadmin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- <link href="<?php echo Url::base(); ?>/css/site.css" rel="stylesheet">
  <link href="<?php echo Url::base(); ?>/css/whatsapp.css" rel="stylesheet"> -->

  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/summernote/summernote-bs4.min.css">
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/daterangepicker/daterangepicker.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo Url::base(); ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">

  <!-- el problema es este jquery -->
  <!-- <script src="<?php echo Url::base(); ?>/librerias/jquery-3.4.1.min.js"></script> -->
  <!-- <script src="<?php echo Url::base(); ?>/js/jquery-3.3.1.slim.min.js"></script> -->

    <!-- <script src="<?php echo Url::base(); ?>/js/jquery-1.11.3.min.js"></script> -->
	<script src="<?php echo Url::base(); ?>/librerias/bootstrap4/popper.min.js"></script>
	<script src="<?php echo Url::base(); ?>/librerias/bootstrap4/bootstrap.min.js"></script>
	<script src="<?php echo Url::base(); ?>/librerias/plotly-latest.min.js" charset="utf-8"></script>
	<script src="<?php echo Url::base(); ?>/librerias/htmlToCanvas.js"></script>
	<script src="js/funciones.js"></script>

  <!-- Bootstrap CSS CDN -->
  <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous"> -->
  <!-- Our Custom CSS -->
  <link href="<?php echo Url::base(); ?>/css/style4.css" rel="stylesheet">

  <?= Html::csrfMetaTags() ?>
</head>
<?php if (!Yii::$app->user->isGuest) { ?>

  <?php 
                $opcion = array();
                $item = array();
                $subitem = array();
                
                $menuperfil = Menuperfil::find()->where(['usuario_idusuario' => yii::$app->user->identity->role ])->all();

                if ($menuperfil) {
                    foreach ($menuperfil as $m) {
                        // OPCION
                        if ($m->opcion && !$m->item && !$m->subitem) {
                            array_push($opcion, $m->opcion);
                        }
                        // ITEM
                        if ($m->opcion && $m->item && !$m->subitem) {
                            array_push($item, $m->item);
                        }
                        // SUBITEM
                        if ($m->opcion && $m->item && $m->subitem) {
                            array_push($subitem, $m->subitem);
                        }
                    }
                    $modelopcion = OpcionMenu::find()->where(['in','id', $opcion ])->orderBy(['orden' => SORT_ASC])->all();
                    $modelitem = ItemMenu::find()->where(['in','id', $item ])->andWhere(['activo' => 1])->all();
                    $modelsubitem = SubitemMenu::find()->where(['in','id', $subitem ])->all();
                    $inc_opcion = 0;


                    foreach ($modelopcion as $o) {
                        // echo $o->nombre . '<br>';
                        foreach ($modelitem as $i) {

                        }
                    }
                    // die();
                    // echo "<pre>";
                    // print_r($modelopcion);
                    // die();
                    foreach ($modelopcion as $o) {
                        $inc_item = 0;
                        foreach ($modelitem as $i) {
                            $inc_subitem = 0;
                            $subitems = [];
                            foreach ($modelsubitem as $s) {
                                //  ===================== SUBITEMS ===============
                                if ($i->id == $s->id_item) {
                                    $subitems[$inc_subitem] =  [
                                                                'label' => $s->etiqueta , 
                                                                'url' => [$s->action],
                                                                'items' => [],
                                                            ];
                                    $inc_subitem++;
                                }
                            }

                            // =========== ITEM ============
                            if ($o->id == $i->id_opcion) {
                                $items[$inc_item] = [
                                                        'label' => $i->etiqueta , 
                                                        'url' => [$i->action],
                                                        'items' => $subitems,
                                                    ];
                                // array_push($itemsarray, $i->idItem->etiqueta);
                                $inc_item++;
                            }
                        }
                        // =========== OPCION ============
                        $menu[$inc_opcion] =  [
                                            'label' => $o->nombre, 
                                            'icono' => $o->icono,
                                            'items' => $items,
                                            ];
                        $inc_opcion++;
                        $items = [];
                    }
                }else{
                    $menu = [];
                }
                // die
        ?>



 <body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <!-- <h5>Software Control</h5> -->
                <!-- <strong>SC</strong> -->
                <center>
                  <p>La tieda web</p>
                </center>
            </div>

            <ul class="list-unstyled components">
                <li class="active">
                  <a href="<?php echo Url::base(); ?>/index.php?r=site%2Findex" class="nav-link">
                    <i class="fas fa fa-home"></i>
                    <span>Inicio</span>
                  </a>
                </li>

                <?php
                
                $contador1 = 0; 
                foreach ($menu as $key => $value) { ?>
                <li>
                    <a href="#pageSubmenu<?php echo $contador1 ?>" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i class="fas fa-<?php echo $value['icono']; ?>"></i>
                        <?php echo $value['label'];  ?>
                    </a>
                    <ul class="collapse list-unstyled" id="pageSubmenu<?php echo $contador1 ?>">
                        <?php  foreach ($value['items'] as $key2 => $value2) { ?>
                          <a href="<?php echo Url::base(); ?>/index.php?r=<?php echo $value2['url'][0] ?>" class="collapse-item">
                            <i class="far fa-circle nav-icon"></i>
                            <span>
                              <?php echo $value2['label'] ?>
                            </span>
                          </a>
                          
                        <?php } ?>
                    </ul>
                </li>
                <?php
                $contador1++;
              } 
              
              ?>
                <!-- <li>
                    
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i class="fas fa-copy"></i>
                        Pages
                    </a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                        <li>
                            <a href="#">Page 1</a>
                        </li>
                        <li>
                            <a href="#">Page 2</a>
                        </li>
                        <li>
                            <a href="#">Page 3</a>
                        </li>
                    </ul>
                </li> -->
                
            </ul>

            <ul class="list-unstyled CTAs">
                <!-- <li>
                    <a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a>
                </li>
                <li>
                    <a href="https://bootstrapious.com/p/bootstrap-sidebar" class="article">Back to article</a>
                </li> -->
                <li >
                  <a class="article" href="#" data-toggle="modal" data-target="#logoutModal" aria-expanded="true" aria-controls="collapsePages">
                    <i class="fas fa-fw fa-window-close"></i>
                    <span>Cerrar Sesión</span>
                  </a>
                </li>
            </ul>
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                    
                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span></span>
                    </button>
                    <!-- <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                        
                    </button> -->
                    <?php echo yii::$app->user->identity->nombres." ".yii::$app->user->identity->apellidos; ?> </span>
                    <img class="img-profile rounded-circle" src="<?php echo Url::base(); ?>/images/fotos/user.png" width="50">

                    <!-- <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Page</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Page</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Page</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Page</a>
                            </li>
                        </ul>
                    </div> -->
                </div>
            </nav>
            <div class="row">
                <div class="col-lg-12 ">
                
                <?php echo $content ?>
              </div>
            </div>
            
        </div>
    </div>

    
</body>
<?php }else{ ?>
      <body id="page-top" style="background-image: url('<?php echo Url::base(); ?>/images/cover.jpg');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-size: cover;">

<?php echo $content ?>

<?php } ?>


<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-white" style="background-color: #3a98ac;">
        <h5 class="modal-title" id="exampleModalLabel">Cerrar Sesión</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span class="text-white" aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Oprima "Salir" si esta completamente seguro de cerrar sesión.</div>
      <?php $form = ActiveForm::begin([
          'method' => 'post',
          'action' => ['site/logout']
        ]); ?>
        <div class="modal-footer">
        
            <div class="col-12 noPadding">
              <div class="col-12 form-group">
                <?= Html::submitButton('Salir', ['class' => 'btn btn-primary btn-block']) ?>
              </div>
              <div class="col-12 form-group">
                <button class="btn btn-default btn-block" type="button" data-dismiss="modal">Cancelar</button>
              </div>
            </div>
          
        </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>

<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo Url::base(); ?>/plugins/jquery/jquery.min.js"></script>

<script src="<?php echo Url::base(); ?>/plugins/jquery-ui/jquery-ui.min.js"></script>


<!-- jQuery CDN - Slim version (=without AJAX) -->
<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
    <!-- Popper.JS -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script> -->
    <!-- Bootstrap JS -->
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script> -->

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });


        
    </script>
<!-- Bootstrap core JavaScript-->
<!-- <script src="<?php echo Url::base(); ?>/sbadmin/vendor/jquery/jquery.min.js"></script> -->
<!-- <script src="<?php echo Url::base(); ?>/sbadmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
<script src="<?php echo Url::base(); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/select2/js/select2.full.min.js"></script>

<!-- Bootstrap core JavaScript-->
<!-- <script src="<?php echo Url::base(); ?>/sbadmin/vendor/jquery/jquery.min.js"></script> -->
<!-- <script src="<?php echo Url::base(); ?>/sbadmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->

<!-- <script src="<?php echo Url::base(); ?>/js/bootstrap-password-toggler.js" type="text/javascript"></script> -->
<!-- Core plugin JavaScript-->
<!-- <script src="<?php echo Url::base(); ?>/sbadmin/vendor/jquery-easing/jquery.easing.min.js"></script> -->

<!-- Custom scripts for all pages-->
<script src="<?php echo Url::base(); ?>/sbadmin/js/sb-admin-2.min.js"></script>

<!-- <script src="<?php echo Url::base(); ?>/js/bootstrap-password-toggler.js" type="text/javascript"></script> -->
<!-- Core plugin JavaScript-->
<!-- <script src="<?php echo Url::base(); ?>/sbadmin/vendor/jquery-easing/jquery.easing.min.js"></script> -->
<!-- Custom scripts for all pages-->
<!-- <script src="<?php echo Url::base(); ?>/sbadmin/js/sb-admin-2.min.js"></script> -->
<!-- PARA PINTAR LAS TABLAS  -->
<script src="<?php echo Url::base(); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/jszip/jszip.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo Url::base(); ?>/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- <script>
        $(function () {
          $('[data-toggle="popover"]').popover()
        });
    </script> -->

</body>

</html>