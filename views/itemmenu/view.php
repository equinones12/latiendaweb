<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ItemMenu */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Item Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-menu-view">
    <div class="card card-primary">
        <!-- Default card contents -->
        <div class="card-header card-header-primary">ITEM MENÚ</div>
        <div class="card-body">

            <p>
                <?= Html::a('<i class="fa fa-fw fa-edit"></i> Actualizar información', ['update', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
                <?= Html::a('<i class="fa fa-fw fa-reply"></i> Volver', ['itemmenu/index'], ['class' => 'btn btn-primary btn-sm']) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'action',
                    'etiqueta',
                    'id_opcion',
                ],
            ]) ?>
        </div>
    </div>
</div>
