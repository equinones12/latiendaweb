<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ItemMenu */

$this->title = 'Atualización';
$this->params['breadcrumbs'][] = ['label' => 'Item Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="item-menu-update">

    <h3 style="color:#ffffff"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
