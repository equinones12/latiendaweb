<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\OpcionMenu;

/* @var $this yii\web\View */
/* @var $model app\models\ItemMenu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-menu-form">

    <?php $form = ActiveForm::begin(); ?>
    <!-- ============== informacion personal -->
    <div class="card card-secondary">
        <div class="card-header card-header-primary">
            <h3 class="card-title">Información de la aplicación </h3>
        </div>
        <!-- /.card-header -->
        <!-- card body -->
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="row">

                        <div class="col-lg-6">
                            <?= $form->field($model, 'etiqueta')->textInput(['maxlength' => true]) ?>        
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'action')->textInput(['maxlength' => true]) ?>        
                        </div>
                    </div>

                </div>
                <div class="col-lg-12 ">
                    <div class="row">

                    
                        <div class="col-lg-6">
                            <?= Html::label('Opcion', 'id_opcion', ['class'=>'control-label']) ?>
                            <?= Html::activeDropDownList(
                                $model,
                                'id_opcion',
                                ArrayHelper::map(OpcionMenu::find()
                                        ->all(), 'id', 'nombre'),
                                [
                                    'class'=>'form-control',
                                    'prompt'=>'[-- Seleccione Opcion --]',
                                    'value'=> '',
                                    'required' => 'required'
                                ]) 
                            ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <br><?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-default btn-sm' : 'btn btn-default btn-sm']) ?>
                    <?= Html::a('<i class="fa fa-fw fa-reply"></i> Volver', ['itemmenu/index'], ['class' => 'btn btn-primary btn-sm']) ?>
                </div>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
