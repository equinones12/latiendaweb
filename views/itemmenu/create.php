<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ItemMenu */

$this->title = 'Registro de Item Menu';
$this->params['breadcrumbs'][] = ['label' => 'Item Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-menu-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
