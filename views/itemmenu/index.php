<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ItemMenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub - Menus';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-menu-index">
   
    
    <div class="card card-primary">
        <!-- Default card contents -->
        <div class="card-header card-header-primary">ITEM MENÚ</div>
        <div class="card-body">
            <p>
                <?= Html::a("<i class='fa fa-check'></i> &nbsp; Crear Item", ['create'], ['class' => 'btn btn-default']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // 'id',
                    'action',
                    'etiqueta',
                    // 'id_opcion',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}{update}',
                        'buttons' => [
                            
                            'view' => function ($url, $model, $key) {
                                // return Html::a('My Action', ['my-action', 'id'=>$model->id]);
                                return Html::a('<span class="fas fa-eye"></span>', ['itemmenu/view', 'id'=>$model->id] );
                            },

                            'update' => function ($url, $model, $key) {
                                // return Html::a('My Action', ['my-action', 'id'=>$model->id]);
                                return Html::a('<span class="fas fa-user-edit"></span>', ['itemmenu/update', 'id'=>$model->id] );
                            },
                        ]
                    ],  
                ],
            ]); ?>
        </div>
    </div>
        
</div>
