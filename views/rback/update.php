<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rback */

$this->title = 'Update Rback: ' . ' ' . $model->idrback;
$this->params['breadcrumbs'][] = ['label' => 'Rbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idrback, 'url' => ['view', 'id' => $model->idrback]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rback-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
