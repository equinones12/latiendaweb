<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Rback */

$this->title = 'ASIGNACION PERMISOS';
$this->params['breadcrumbs'][] = ['label' => 'Rbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rback-create">
    <?= $this->render('_form', [
        'model' => $model,
        'id' => $id,
        'idRol' => $idRol,
        'rol' => $rol,
        'controlador' => $controlador,
    ]) ?>

</div>
