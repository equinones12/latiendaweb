<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'RBAC';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rback-index">

    <h3 class="alert alert-info"><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Controlador', ['controladores/create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Crear Accion', ['acciones/create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading">PANEL DE MENUS</div>
        <div class="panel-body">

            <?php foreach ($roles as $r) { ?>
                <h3 class="alert col-lg-12 alert-danger noPadding"><?php echo 'Rol ' . $r->nombre; ?></h3>
                <?php foreach ($controlador as $c) { ?>
                    <div class="col-lg-12 alert alert-info noPadding">
                        <p style="float:left; margin-left:20px;"><?php echo $c->nombreControlador; ?></p>
                        <?= Html::a('Crear RBAC', ['rback/create', 'id' => $c->idcontrolador, 'idRol' => $r->id], ['class' => 'btn btn-primary btn-xs pull-right']) ?>
                    </div>
                    <?php foreach ($rback as $rb) { ?>
                        <?php if ($r->id == $rb->idRol && $rb->idControlador == $c->idcontrolador) { ?>
                            <div class="col-lg-11 col-lg-offset-1  noPadding  alert alert-success">
                                <p style="margin-left: 20px; width:150px; float:left;"><?php echo $rb->idAccion0->nombreAccion; ?></p>
                                <?php if ($rb->estadoRback == 1) { ?>
                                    <input id="<?php echo $rb->idrback ?>" type="checkbox" class="switchRbac" checked >
                                <?php }else{ ?>
                                    <input id="<?php echo $rb->idrback ?>" type="checkbox" class="switchRbac" >
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>
