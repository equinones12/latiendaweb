<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Acciones;

/* @var $this yii\web\View */
/* @var $model app\models\Rback */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rback-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'idRol')->textInput(['value' => $idRol, 'type' => 'hidden'])->label(false) ?>
    <?= $form->field($model, 'idControlador')->textInput(['value' => $id, 'type' => 'hidden'])->label(false) ?>
    <div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading">ASIGNACION DE ACCIONES</div>
        <div class="panel-body">


            <div class="col-lg-12 noPadding form-group">
            	<div class="col-lg-6">
            		<label for="" class="label-form">Rol</label> <br> 
            		<?php echo $rol->nombre; ?>
            	</div>
            	<div class="col-lg-6">
            		<label for="" class="label-form">Controlador</label> <br> 
            		<?php echo $controlador->nombreControlador; ?>
            	</div>
            </div>

            <div class="col-lg-12 noPadding form-group">
            	<div class="col-lg-6">
            	    <?= Html::label('Accion', 'idAccion', ['class'=>'control-label']) ?>
            	    <?= Html::activeDropDownList(
            	        $model,
            	        'idAccion',
            	        ArrayHelper::map(Acciones::find()
            	                ->all(), 'idaccion', 'nombreAccion'),
            	        [
            	            'class'=>'form-control',
            	            'prompt'=>'[-- Seleccione Accion --]',
            	            'value'=> '',
            	            'required' => 'required'
            	        ]) 
            	    ?>
            	    <div class="help-block"></div>
            	</div>
            	<div class="col-lg-6">
            	    <?= Html::label('Estado RBAC', 'estadoRback', ['class'=>'control-label']) ?>
            	    <?= Html::activeDropDownList(
            	        $model,
            	        'estadoRback',
            	        [
            	        	1 => 'Activo',
            	        	0 => 'Inactivo'
            	        ],
            	        [
            	            'class'=>'form-control',
            	            'value'=> '',
            	            'required' => 'required'
            	        ]) 
            	    ?>
            	    <div class="help-block"></div>
            	</div>
                <div class="col-lg-6">
                    <div class="form-group">
                      <br><?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>

            </div>

            
        </div>
    </div>    
    <?php ActiveForm::end(); ?>

</div>
