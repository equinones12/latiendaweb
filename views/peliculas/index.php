<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Peliculas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="peliculas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Peliculas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <table class="table table-bordered table-hover table-striped">
        <thead>
            <th>id</th>
            <th>Nombre</th>
            <th>Categoria</th>
            <th>Autor</th>
            <th>Fecha de entrega</th>
            <th>Productora</th>
            <th>Acciones</th>
        </thead>
        <tbody>
            <?php  foreach($peliculas as $model) { ?>
                <tr>
                    <td><?php echo $model->id_pelicula?></td>
                    <td><?php echo $model->nombre?></td>
                    <td><?php echo $model->categoria->nombre?></td>
                    <td><?php echo $model->autor->nombre?></td>
                    <td><?php echo $model->fecha_lanzamiento; ?></td>
                    <td><?php echo $model->productora; ?></td>
                    <td>
                        <?= Html::a('<i class="fa fa-edit"></i>', ['update', 'id_pelicula' => $model->id_pelicula], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('<i class="fa fa-trash"></i>', ['Delete', 'id_pelicula' => $model->id_pelicula], ['class' => 'btn btn-danger']) ?>
                        <?= Html::a('<i class="fa fa-eye"></i>', ['view', 'id_pelicula' => $model->id_pelicula], ['class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
            <?php } ?>    
        </tbody>                
    </table>


</div>
