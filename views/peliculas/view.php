<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\models\Autores;

/* @var $this yii\web\View */
/* @var $model app\models\Peliculas */

$this->title = $model->id_pelicula;
$this->params['breadcrumbs'][] = ['label' => 'Peliculas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="peliculas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id_pelicula' => $model->id_pelicula], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id_pelicula' => $model->id_pelicula], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'get',
            ],
        ]) ?>
    </p>

    <table class="table table-bordered table-hover table-striped">
                <thead>
                    <th>id</th>
                    <th>Nombre</th>
                    <th>Categoria</th>
                    <th>Autor</th>
                    <th>Fecha de entrega</th>
                    <th>Productora</th>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $model->id_pelicula?></td>
                        <td><?php echo $model->nombre?></td>
                        <td><?php echo $model->categoria->nombre?></td>
                        <td><?php echo $model->autor->nombre?></td>
                        <td><?php echo $model->fecha_lanzamiento; ?></td>
                        <td><?php echo $model->productora; ?></td>
                    </tr>
                </tbody>                
            </table>

</div>
