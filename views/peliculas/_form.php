<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Categorias;
use app\models\Autores;


/* @var $this yii\web\View */
/* @var $model app\models\Peliculas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="peliculas-form">

    <?php $form = ActiveForm::begin(); ?>
    <label for="">Categoria</label>
    <div class="form-group">
        <?= Html::activeDropDownList(
            $model,
            'id_categoria',
            ArrayHelper::map(Categorias::find()->all(), 'id_categoria','nombre'),
            [
                'class'=>'form-control',
                'prompt'=>'[-- Seleccione categoria--]',
                // 'value'=> '',
                'required' => 'required'
            ]) 
        ?>
    </div> 
    <label for="">Autor</label>
    <div class="form-group">
        <?= Html::activeDropDownList(
            $model,
            'id_autor',
            ArrayHelper::map(Autores::find()->all(), 'id_autor','nombre'),
            [
                'class'=>'form-control',
                'prompt'=>'[-- Seleccione autor--]',
                // 'value'=> '',
                'required' => 'required'
            ]) 
        ?>
    </div>    

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
    <label for="">Fecha de lanzamiento</label>
    <div class="form-group">
      <input type="date" name="Peliculas[fecha_lanzamiento]" class="form-control" value="<?php echo $model->fecha_lanzamiento?>">
    </div>
    <?= $form->field($model, 'productora')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
