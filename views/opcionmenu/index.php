<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OpcionMenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Opcion Menus';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcion-menu-index">

   
    <div class="card card-primary">
        <!-- Default card contents -->
        <div class="card-header card-header-primary">OPCIÓN MENÚ</div>
        <div class="card-body">
    
            <p>
                <?= Html::a("<i class='fa fa-check'></i> &nbsp;Create Opción Menú", ['create'], ['class' => 'btn btn-dafult']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'nombre',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}{update}',
                        'buttons' => [
                            
                            'view' => function ($url, $model, $key) {
                                // return Html::a('My Action', ['my-action', 'id'=>$model->id]);
                                return Html::a('<span class="fas fa-eye"></span>', ['opcionmenu/view', 'id'=>$model->id] );
                            },

                            'update' => function ($url, $model, $key) {
                                // return Html::a('My Action', ['my-action', 'id'=>$model->id]);
                                return Html::a('<span class="fas fa-user-edit"></span>', ['opcionmenu/update', 'id'=>$model->id] );
                            },
                        ]
                    ],  
                ],
            ]); ?>
        </div>
    </div>
</div>
