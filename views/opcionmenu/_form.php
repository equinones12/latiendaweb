<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OpcionMenu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="opcion-menu-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<div class="card ">
      	<!-- Default card contents -->
		<div class="card-heading card-header-primary">Opción Menú</div>
		<div class="card-body">

		    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

		    <div class="form-group">
		        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-default' : 'btn btn-default']) ?>
				<?= Html::a('<i class="fa fa-fw fa-reply"></i> Volver', ['opcionmenu/index'], ['class' => 'btn btn-primary btn-sm']) ?>
		    </div>
		</div>
	</div>

    <?php ActiveForm::end(); ?>

</div>
