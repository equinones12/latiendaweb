<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OpcionMenu */

$this->title = 'Update Opcion Menu: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Opcion Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="opcion-menu-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
