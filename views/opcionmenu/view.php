<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OpcionMenu */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Opcion Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcion-menu-view">
    
    <div class="card ">
        <!-- Default card contents -->
        <div class="card-header card-header-primary">Opción Menú</div>
        <div class="card-body">
            <p>
                
                <?= Html::a('<i class="fa fa-fw fa-edit"></i> Actualizar información', ['update', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
                <?= Html::a('<i class="fa fa-fw fa-reply"></i> Volver', ['opcionmenu/index'], ['class' => 'btn btn-primary btn-sm']) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'nombre',
                ],
            ]) ?>
        </div>
    </div>
</div>
