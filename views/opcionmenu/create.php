<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OpcionMenu */

$this->title = 'Create Opcion Menu';
$this->params['breadcrumbs'][] = ['label' => 'Opcion Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcion-menu-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
