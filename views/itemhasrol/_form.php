<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\ItemMenu;

/* @var $this yii\web\View */
/* @var $model app\models\ItemHasRol */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-has-rol-form">
    
    <?php if (!$model->isNewRecord) {
        $item = ItemMenu::find()->where(['id' => $model->id_item])->One();
    } ?>

    <?php $form = ActiveForm::begin(); ?>


    <div class="panel panel-primary">
    	<div class="panel-heading">Informacion del Item</div>
    	<div class="panel-body">
    		    <div class="col-lg-12 noPadding">
                    <div class="col-lg-6">
                        <?= Html::label('Etiqueta', 'id_item', ['class'=>'control-label']) ?>
                        <?= Html::activeDropDownList(
                            $model,
                            'id_item',
                            ArrayHelper::map(ItemMenu::find()
                                    ->where(['id_opcion'=>$opcion])
                                    ->all(), 'id', 'etiqueta'),
                            [
                                'class'=>'form-control',
                                'prompt'=>'[-- Seleccione Etiqueta --]',
                                'value'=> '',
                                'required' => 'required'
                            ]) 
                        ?>
                    </div>
    		    </div>
    			<div class="col-lg-12 noPadding">
    				<div class="col-lg-6">
    				    <?= Html::label('Estado', 'estado', ['class'=>'control-label']) ?>
    				    <?= Html::activeDropDownList(
    				        $model,
    				        'estado',
    				        [
    				            1 => 'Activo',
    				            0 => 'Inactivo'
    				        ],
    				        [
    				            'class'=>'form-control',
    				            'prompt'=>'[-- Seleccione Estado --]',
    				            'value'=> '',
    				            'required' => 'required'
    				        ]) 
    				    ?>
    				    <div class="help-block"></div>
    				</div>

    				<div class="col-lg-6">
    				    <?= $form->field($model, 'orden')->textInput(['type' => 'number','min'=>0]) ?>
    				</div>
    			</div>
    			<div class="col-lg-12">
    				<label class="control-label">Rol</label><br>
    				<label class="control-label"><?php echo $rol->nombre ?></label>
    			    <?= $form->field($model, 'id_rol')->hiddenInput(['value'=> $rol->id])->label(false) ?>
                    <?php if ($model->isNewRecord) { ?>
                        <input type="hidden" name="opcion" value="<?php echo $opcion; ?>">
                    <?php } ?>
    			</div>
    	</div>
    </div>




	



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
