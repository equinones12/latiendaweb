<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ItemHasRol */

$this->title = 'Update Item Has Rol: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Item Has Rols', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="item-has-rol-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'rol' => $rol,
        'opcion' => $opcion,
    ]) ?>

</div>
