<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ItemHasRolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Item Has Rols';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-has-rol-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Item Has Rol', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_item',
            'id_rol',
            'estado',
            'orden',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
