<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ItemHasRol */

$this->title = 'Crear Item';
$this->params['breadcrumbs'][] = ['label' => 'Menu', 'url' => ['opcionhasrol/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-has-rol-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'rol' => $rol,
        'opcion' => $opcion,
    ]) ?>

</div>
