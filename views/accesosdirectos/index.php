<?php

use yii\widgets\ActiveForm;

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AccesosDirectosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Accesos Directos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accesos-directos-index">
    
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h3 class="card-title"><?php echo $this->title; ?></h3>
                <!--<h4 class="card-category">New employees on 15th September, 2016</h4>-->
            </div>
            <div class="card-body table-responsive">
                <div class="row">
                    <div class="col-lg-3 form-group">
                        <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#almacen">PACIENTES</button>
                    </div>
                    <div class="col-lg-3 form-group">
                        <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#compras">MEDICOS</button>
                    </div>
                </div>
            </div>
        


<?php $form = ActiveForm::begin([
    'action' => ['guardarcambios']
]); ?>


    
 <!-- Modal almacen -->
    <div id="almacen" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">PACIENTES</h4>
                </div>
                <div class="modal-body">
                    <ul>
                        <?php foreach ($roles as $r) { 
                            $checked = '';
                            foreach ($accesosdirectos as $a) {
                                if ($r->id == $a->idRol && $a->numerotapa == 5) {
                                    $checked = 'checked';
                                }
                            } ?>
                            <li> <input type="checkbox" <?php echo $checked; ?> name="almacen[]" value="<?php echo $r->id ?>"> <?php echo $r->nombre ?></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>




<!-- Modal compras -->
    <div id="compras" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">MEDICOS</h4>
                </div>
                <div class="modal-body">
                    <ul>
                        <?php foreach ($roles as $r) { 
                            $checked = '';
                            foreach ($accesosdirectos as $a) {
                                if ($r->id == $a->idRol && $a->numerotapa == 8) {
                                    $checked = 'checked';
                                }
                            } ?>
                            <li> <input type="checkbox" <?php echo $checked; ?> name="compras[]" value="<?php echo $r->id ?>"> <?php echo $r->nombre ?></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group col-lg-12 ">
        <br>
        <?= Html::submitButton('<i class="fa fa-fw fa-check "></i> Guardar cambios', ['class' => 'btn btn-primary btn-sm']) ?>
        <br><br>
    </div>
    <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>