<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OpcionHasRol */

$this->title = 'Asignación de Item a Menú';
$this->params['breadcrumbs'][] = ['label' => 'Menu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcion-has-rol-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
