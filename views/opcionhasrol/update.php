<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OpcionHasRol */

$this->title = 'Actualizar opcion: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'menu', 'url' => ['opcionhasrol/index']];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="opcion-has-rol-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
