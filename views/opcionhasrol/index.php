<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OpcionHasRolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Configuraciones de Menu';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcion-has-rol-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <!-- <?= Html::a('Asignar Opcion', ['create'], ['class' => 'btn btn-danger']) ?> -->
    </p>
    <div class="card card-primary">
        <!-- Default card contents -->
        <div class="card-header card-header-primary">Configuración de menus</div>
        <div class="card-body">
            <p class="alert alert-info">
                <?= Html::a('<i class="fa fa-fw fa-plus"></i>Crear Menu', ['opcionmenu/index'], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('<i class="fa fa-fw fa-plus"></i>Crear Sub - Menu', ['itemmenu/index'], ['class' => 'btn btn-primary']) ?>
            </p>
        </div>
    </div>
</div>
