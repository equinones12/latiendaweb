<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Rol;
use app\models\OpcionMenu;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\OpcionHasRol */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="opcion-has-rol-form">

    <?php $form = ActiveForm::begin(); ?>

        <?php if (!$model->isNewRecord) {
            $opcion = OpcionMenu::find()->where(['id' => $model->id_opcion])->One();
        } ?>

    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title">Información de la aplicación </h3>
        </div>
        <!-- /.card-header -->
        <!-- card body -->
        <div class="card-body">
            <div class="row">
                <div class="form-group col-lg-6">
                    <?= Html::label('Etiqueta', 'id_opcion', ['class'=>'control-label']) ?>
                    <?= Html::activeDropDownList(
                        $model,
                        'id_opcion',
                        ArrayHelper::map(OpcionMenu::find()
                                ->all(), 'id', 'nombre'),
                        [
                            'class'=>'form-control',
                            'prompt'=>'[-- Seleccione Etiqueta --]',
                            'value'=> '',
                            'required' => 'required'
                        ]) 
                    ?>
                </div>

                <div class="form-group col-lg-6">
                    <?= Html::label('Rol', 'id_rol', ['class'=>'control-label']) ?>
                    <?= Html::activeDropDownList(
                        $model,
                        'id_rol',
                        ArrayHelper::map(Rol::find()
                                ->all(), 'id', 'nombre'),
                        [
                            'class'=>'form-control',
                            'prompt'=>'[-- Seleccione Rol --]',
                            'value'=> '',
                            'required' => 'required'
                        ]) 
                    ?>
                    <div class="help-block"></div>
                </div>

                <div class="form-group col-lg-6">
                    <?= Html::label('Estado', 'estado', ['class'=>'control-label']) ?>
                    <?= Html::activeDropDownList(
                        $model,
                        'estado',
                        [
                            1 => 'Activo',
                            0 => 'Inactivo'
                        ],
                        [
                            'class'=>'form-control',
                            'prompt'=>'[-- Seleccione Estado --]',
                            'value'=> '',
                            'required' => 'required'
                        ]) 
                    ?>
                    <div class="help-block"></div>
                </div>

                <div class="col-lg-6">
                    <?= $form->field($model, 'orden')->textInput(['type' => 'number','min'=>0]) ?>
                </div>

                <div class="form-group col-lg-12">
                    <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>

    	

    <?php ActiveForm::end(); ?>

</div>
