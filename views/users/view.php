<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\TiposDetalles;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = "Información del Usuario registrado";
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

// echo "<pre>";
// print_r($model);
// die;

?>
<div class="col-md-12">

    
<div class="card">
    <div class="card-header card-header-primary">
        <h3 class="card-title"><?php echo $this->title; ?></h3>
    </div>
    <div class="card-body table-responsive">
        <div class="col-lg-12">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-usuario" role="tab" aria-controls="nav-home" aria-selected="true">Usuario</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-usuario">
                <!--   PACIENTE -->
                
                <div class="tab-pane fade show active" id="custom-tabs-five-overlay" role="tabpanel" aria-labelledby="custom-tabs-five-overlay-tab">
                    <div class="overlay-wrapper">
                        <!-- <div class="overlay"><i class="fas fa-3x fa-sync-alt fa-spin"></i><div class="text-bold pt-2">Loading...</div></div> -->
                        <div class="col-lg-12 alert alert-primary text-center">Información Personal</div>
                        <table class="table table-striped table-bordered">
                            <tbody>
                                <tr>
                                <td><strong>Nombres: </strong><?php echo $model->nombres; ?></td>
                                <td><strong>Apellidos: </strong><?php echo $model->apellidos; ?></td>
                                </tr>
                                <tr>
                                <td><strong>Identificacion: </strong><?php echo $model->numeroIdentificacion; ?></td>
                                </tr>
                                <tr>
                                <td><strong>Direccion: </strong><?php echo $model->direccion; ?></td>
                                <td><strong>Telefono: </strong><?php echo $model->telefono; ?></td>
                                </tr>
                                <tr>
                                <td ><strong>Email: </strong><?php echo $model->email; ?></td>
                                
                                </tr>
                                <tr>
                                <!-- <td ><strong>Telefono: </strong><?php echo $model->telefono; ?></td> -->
                                </tr>
                            </tbody>
                        </table>
                        <div class="col-lg-12 alert alert-primary text-center">Información Del Usuario</div>
                        <table class="table table-striped table-bordered">
                            <tbody>
                                <tr>
                                <td><strong>Username: </strong><?php echo $model->username; ?></td>
                                <td><strong>Rol usuario: </strong><?php echo $model->rol->nombre; ?></td>
                                
                                
                                </tr>
                                
                            </tbody>
                        </table>
                       
                    </div>
                </div>
                

                <div class="tab-pane fade" id="nav-citas" role="tabpanel" aria-labelledby="nav-contact-tab">

                </div>
            </div>
            <?= Html::a('<i class="fa fa-fw fa-edit"></i> Actualizar información', ['update', 'id' => $model->id], ['class' => 'btn btn-default btn-sm ']) ?>
            <?= Html::a('<i class="fa fa-fw fa-reply"></i> Volver', ['index'], ['class' => 'btn btn-primary btn-sm ']) ?>
        </div>
    </div>
</div>



<script type="text/javaScript">


$('.btn-eliminar-documento').click(function(){

  $('#nameFileRemove').text($(this).siblings('a').children('label').text());
  $('#btn-ajax-delete-documento').removeAttr('data-id');
  $('#btn-ajax-delete-documento').attr('data-id',$(this).data('id') );
  // $(this).parent().remove();
  $('#modalRemoveDocument').modal('show');

});


$('#btn-ajax-delete-documento').click(function(){

  var id = $(this).attr('data-id');

  $.ajax({
      url: '<?php echo Yii::$app->request->baseUrl ?>/?r=users/eliminardocumento',
      type: 'get',
      data: {
          id : id , 
      },
      success: function (data) {
          $('#'+id).remove();
          $('#modalRemoveDocument').modal('hide');
          // alert(data);
      }
  }); // fin ajax
});

</script>