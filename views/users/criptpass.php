<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Rol;
use kartik\date\DatePicker;

 ?>
<?php $form = ActiveForm::begin([
	'method' => 'POST',
	'action' => ['users/criptpass']
])?>
	<label class="label-control">Digite pass</label>
	<input type="password" name="pass" class="form-control" required>
	<br>
	<div class="form-group">
	    <?= Html::submitButton('criptpass', ['class' =>'btn btn-primary']) ?>
	</div>
<?php ActiveForm::end(); ?>
<?php if ($pass) { ?>
	<h3 class="alert alert-info"><?php echo $pass; ?></h3>
<?php } ?>