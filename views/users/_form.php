<?php

use app\models\Cargos;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Rol;
use app\models\TiposDetalles;
use kartik\date\DatePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>


    <!-- ============== informacion personal -->
    <div class="card card-secondary">
      <div class="card-header card-header-primary">
        <h3 class="card-title">Información Personal</h3>
      </div>
      <!-- /.card-header -->
      <!-- card body -->
      <div class="card-body">
        <div class="row">
          
          <div class="col-lg-6 form-group">
            <?= $form->field($model, "nombres") ?>   
          </div>
          <div class="col-lg-6 form-group">
            <?= $form->field($model, "apellidos") ?>   
          </div>
        
        
          <!-- <div class="col-lg-6 form-group">
            <?php if ($model->isNewRecord) { ?>
              <?= $form->field($model, "numeroIdentificacion") ?>  
            <?php }else{ ?>
              <?= $form->field($model, "numeroIdentificacion") ?>  
            <?php } ?> 
          </div> -->
        
        
          <div class="col-lg-6 form-group">
            <?= $form->field($model, "direccion") ?>   
          </div> 

           <div class="col-lg-6 form-group">
            <?= $form->field($model, "telefono") ?>   
          </div>
        
        
          <div class="col-lg-6 form-group">
              <?= $form->field($model, "email")->input("email") ?>   
          </div>
          
        
        
          
          
        </div>
      </div>
    </div>
    <!-- ============== informacion laboral -->
    <div class="card">
      <div class="card-header card-header-primary">
        <h3 class="card-title">Información del Usuario </h3>
      </div>
      <!-- /.card-header -->
      <!-- card body -->
      <div class="card-body">
        <div class="row">
            
          <div class="col-lg-6 form-group">
            <?= $form->field($model, 'username') ?>

          </div>

          <div class="col-lg-6 form-group">
              <?= $form->field($model, "password")->input("password") ?>   
          </div>



          <div class="col-lg-6 form-group">
            

            <!-- <?= Html::label('Rol Usuario', 'tipoContrato', ['class'=>'control-label']) ?> -->
            <?php
            
              $var = \yii\helpers\ArrayHelper::map(Rol::find()->all(), 'id', 'nombre');
              echo $form->field($model, 'role')->dropDownList($var, ['prompt' => 'Seleccione uno', 'class' => 'form-control select2bs4'])->label('Rol Usuario'); 
            ?>

          </div>
      
      
          <div class="col-lg-6 form-group">
              <?php 
                  $var = [  1 => 'Activo',  0 => 'Inactivo'];
                  echo $form->field($model, 'activate')->dropDownList($var, ['prompt' => 'Seleccione uno', 'class' => 'form-control select2bs4'])->label('Estado Usuario'); 
              ?>
          </div>
          
          
          
          
        </div>
      </div>
    </div>
    <!-- ============== MENU ASOCIADO AL PERFIL ============ -->
    
    <div class="card card-secondary " id="demo">
      <div class="card-header card-header-primary">Menus Asociados al perfil</div>
      <div class="card-body">
            <div class="checkbox pull-right">
              <label><input  type="checkbox"  checked  id="selectAll" ><strong>Seleccionar todo</strong></label>
            </div>
        <br>
        <br>
        <?php foreach ($opciones as $o) { ?>
          <div class="checkbox">
            <?php if (in_array($o->id,$useropcion)) { ?>
              <?php $checkedOpcion = 'checked'; ?>
            <?php }else{ ?>
              <?php $checkedOpcion = ''; ?>
            <?php } ?>
            <label><input class="opcionesCheck"  type="checkbox" name="opciones[]" value="<?php echo $o->id.',0,0' ?>" <?php echo $checkedOpcion; ?>  id="<?php echo $o->id; ?>" value=""><?php echo $o->nombre; ?></label>
          </div>
          <div style="margin-left: 20px;">
            <?php foreach ($items as $i) { ?>
              <?php if ($o->id == $i->id_opcion) { ?>
                <div class="checkbox">
                  <?php if (in_array($i->id,$useritem)) { ?>
                    <?php $checkedItem = 'checked'; ?>
                  <?php }else{ ?>
                    <?php $checkedItem = ''; ?>
                  <?php } ?>
                  <label><input type="checkbox" name="opciones[]" value="<?php echo $o->id.','.$i->id.',0' ?>" class="itemsCheck" id="<?php echo $i->id; ?>" <?php echo $checkedItem; ?>  data-opcion="<?php echo $o->id; ?>" value=""><?php echo $i->etiqueta; ?></label>
                </div>

                <div style="margin-left: 20px;">
                  <?php foreach ($subitems as $s) { ?>
                    <?php if ($i->id == $s->id_item) { ?>
                      <div class="checkbox">
                        <?php if (in_array($s->id,$usersubitem)) { ?>
                          <?php $checkedSubitem = 'checked'; ?>
                        <?php }else{ ?>
                          <?php $checkedSubitem = ''; ?>
                        <?php } ?>
                        <label><input type="checkbox" name="opciones[]" value="<?php echo $o->id.','.$i->id.','.$s->id ?>" class="subitemsCheck" <?php echo $checkedSubitem; ?>  data-opcion="<?php echo $o->id; ?>" data-item="<?php echo $i->id; ?>"><?php echo $s->etiqueta; ?></label>
                      </div>
                    <?php } ?>
                  <?php } ?>
                </div>
              <?php } ?>
            <?php } ?>
          </div>
        <?php } ?>
      </div>
    </div>

    <div class="col-lg-12 noPadding form-group">
      <!-- <button type="button" class="btn btn-dark " data-toggle="collapse" data-target="#demo"><i class="fa fa-folder-open"></i>Menu</button> -->
      <?= Html::submitButton($model->isNewRecord ? 'Crear Usuario' : '<i class="fa fa-fw fa-edit"></i> Editar Usuario', ['class' => $model->isNewRecord ? 'btn btn-default btn-sm' : 'btn btn-default btn-sm', ]) ?>
      <?= Html::a('<i class="fa fa-fw fa-reply"></i> Volver', ['index'], ['class' => 'btn btn-primary btn-sm ']) ?>
      
             
    </div>

    

    <?php ActiveForm::end(); ?>

</div>
<!-- ================ JS =========== -->
<script>
  $(document).ready(function(){


    function Validar(){


    }

    $(".opcionesCheck").click(function(){
      if (!$(this).is(':checked')) {
        $(document).find("[data-opcion='" + $(this).attr('id') + "']").prop('checked', false);   
      }
    });

    $(".itemsCheck").click(function(){
      if (!$(this).is(':checked')) {
        $(document).find("[data-item='" + $(this).attr('id') + "']").prop('checked', false);   
      }else{
        $('#'+$(this).data('opcion')).prop('checked', true);
      }
    }); 

    $(".subitemsCheck").click(function(){
      if ($(this).is(':checked')) {
        $('#'+$(this).data('opcion')).prop('checked', true);
        $('#'+$(this).data('item')).prop('checked', true);
      }
    });

    $("#selectAll").click(function(){
      if ($(this).is(':checked')) {
        $(".opcionesCheck").prop('checked', true);
        $(".itemsCheck").prop('checked', true);
        $(".subitemsCheck").prop('checked', true);
      }else{
        $(".opcionesCheck").prop('checked', false);
        $(".itemsCheck").prop('checked', false);
        $(".subitemsCheck").prop('checked', false);
      }
    });

    $(".addRole").click(function(){
      if ($(this).hasClass('select')) {
        $(this).removeClass('select');
        $(this).addClass('btn-primary');
        $(this).removeClass('btn-success');
      }else{
        $(this).addClass('btn-success');
        $(this).addClass('select')
        $(this).removeClass('btn-primary');
      }
      validateRoleActive();
    });
  })
  // =============== FUNCIONES ==================
  function validateRoleActive(){
    var concatRole = '';
    var init = false;
    $(".addRole").each(function(){
      console.log($(this).attr('class'));
      if ($(this).hasClass('select')) {
        if (init) {
          concatRole = concatRole + ',' + $(this).attr('id');
        }else{
          concatRole = $(this).attr('id');
          init = true;
        }
      }
    });
    $("#users-role").val(concatRole);
  }
</script>