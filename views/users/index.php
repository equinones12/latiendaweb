<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Usuarios del Sistema';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="<?php echo Url::base(); ?>/librerias/jquery-3.4.1.min.js"></script>
    <!-- /.card-header -->
<div class="users-index">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h3 class="card-title"><?php echo $this->title; ?></h3>
                <!--<h4 class="card-category">New employees on 15th September, 2016</h4>-->
            </div>
            <div class="card-body table-responsive">
                <div class="box-tools ">
                    <p>
                        <?= Html::a("<i class='fa fa-user'></i> &nbsp;Crear Usuario", ['site/register'], ['class' => 'btn btn-default']) ?>  
                    </p>
                </div>        
                <div class="col-lg-12">
                    <div class="row">
                    <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                            <th>Username</th>
                            <th>nombres</th>
                            <th>apellidos</th>
                            <th>No. identificación</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Dirección</th>
                            <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($model as $key => $value) { ?>
                            <tr>
                                <td><?php echo $value['username'] ?></td>
                                <td><?php echo $value['nombres'] ?></td>
                                <td><?php echo $value['apellidos'] ?></td>
                                <td><?php echo $value['numeroIdentificacion'] ?></td>
                                <td><?php echo $value['email'] ?></td>
                                <td><?php echo strtoupper( $value['nombre']) ?></td>
                                <td><?php echo $value['direccion'] ?></td>
                                <td>
                                    <?= Html::a('<i class="fa fa-fw fa-eye"></i> Ver', ['users/view', 'id'=> $value['usuario']], ['class' => 'btn btn-primary btn-block btn-xs ']) ?>
                                    <?= Html::a('<i class="fa fa-fw fa-edit"></i> Editar', ['users/update', 'id'=> $value['usuario']], ['class' => 'btn btn-primary btn-block btn-xs text-white ']) ?>
                                <?php if ($value['activate']==1) { ?>
                                    <?= Html::a('<i class="fa fa-fw fa-trash"></i> Inactivar', ['users/inactivar', 'id'=> $value['usuario']], ['class' => 'btn btn-danger btn-block btn-xs text-white ']) ?>
                                <?php }else{ ?>
                                    <?= Html::a('<i class="fa fa-fw fa-check "></i> Activar', ['users/activar', 'id'=> $value['usuario']], ['class' => 'btn btn-success btn-block btn-xs text-white ']) ?>
                                <?php } ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
  $(function () {
    $("#example1").DataTable({
    
    language: {
      "processing": "Procesando...",
      "lengthMenu": "Mostrar _MENU_ registros",
      "zeroRecords": "No se encontraron resultados",
      "emptyTable": "Ningún dato disponible en esta tabla",
      "info":           "Mostrando _START_ a _END_ de _TOTAL_ Elementos",
      "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
      "infoFiltered": "(filtrado de un total de _MAX_ registros)",
      "search": "Buscar:",
      "infoThousands": ",",
      "loadingRecords": "Cargando...",
      "paginate": {
          "first": "Primero",
          "last": "Último",
          "next": "Siguiente",
          "previous": "Anterior"
      },
      "aria": {
          "sortAscending": ": Activar para ordenar la columna de manera ascendente",
          "sortDescending": ": Activar para ordenar la columna de manera descendente"
      },
        
        
    },
      "responsive": true, "lengthChange": true, "autoWidth": false,
      // "dom": 'Bfrtip',
      "buttons": [
            {
                "extend": 'copyHtml5',
                "exportOptions": {
                    "columns": [ 0, ,1,2,3,4,5,6 ]
                }
            },
            {
                "extend": 'excelHtml5',
                "exportOptions": {
                    "columns": [0,1,2,3,4,5,6]
                }
            },
            {
                "extend": 'pdfHtml5',
                "exportOptions": {
                    "columns": [ 0, 1, 2, 3,4,5,6 ]
                }
            },
            // 'colvis'
        ]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');


    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>