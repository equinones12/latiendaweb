<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Actualización de Usuario No: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->numeroIdentificacion, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="users-update">

    <!-- <div class="jumbotron">
        <img width="150" src="<?php echo Url::base(); ?>/images/fotos/<?php echo $model->foto; ?>" alt="..." class="img-circle">
        <p class="lead" style="color:#ffffff"><?php echo $model->nombres.' '.$model->apellidos; ?></p>
    </div> -->

    <?= $this->render('_form', [
        'model' => $model,
        'menuperfil' => $menuperfil,
        'useropcion' => $useropcion,
        'useritem' => $useritem,
        'usersubitem' => $usersubitem,
        'opciones' => $opciones,
    	'items' => $items,
    	'subitems' => $subitems,
    ]) ?>

</div>
