<?php

namespace app\controllers;

use Yii;
use yii\db\Query;
use app\models\Rol;
use yii\web\Controller;
use app\models\ItemMenu;
use app\models\RolSearch;
use app\models\Menuperfil;
use app\models\OpcionMenu;
use app\models\SubitemMenu;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * RolController implements the CRUD actions for Rol model.
 */
class RolController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['get'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rol models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RolSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // $model = Rol::find()->all();

        $query = new Query();
        $connection = Yii::$app->db;

        // consulta de los trabajos cotizados
        $query = $connection->createCommand('SELECT *, (select count(*) from users where users.role = rol.id) as validacion

                                            from    rol
                                            where   1=1
                                            ');
        $model = $query->queryAll();
        // echo "<pre>";
        // print_r($detalle);
        // die;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,

        ]);
    }

    /**
     * Displays a single Rol model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Rol model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Rol();

        $roles = Rol::find()->all();
        $menuperfil = MenuPerfil::find()->all();
        
        $useropcion = array();
        $useritem = array();
        $usersubitem = array();


        if ($menuperfil) {
            foreach ($menuperfil as $m) {
                // OPCION
                if ($m->opcion && !$m->item && !$m->subitem) {
                    array_push($useropcion, $m->opcion);
                }
                // ITEM
                if ($m->opcion && $m->item && !$m->subitem) {
                    array_push($useritem, $m->item);
                }
                // SUBITEM
                if ($m->opcion && $m->item && $m->subitem) {
                    array_push($usersubitem, $m->subitem);
                }
            }
        }

        $opciones = OpcionMenu::find()->all();
        $items = ItemMenu::find()->all();
        $subitems = SubitemMenu::find()->all();

        if ($model->load(Yii::$app->request->post()) ) {

            

            if ($model->save()) {

                if (isset($_POST['opciones'])) {
                    foreach ($_POST['opciones'] as $opcion) {
                        
                        $opcionExplode = explode(',', $opcion);
                        $opcion = $opcionExplode[0];
                        $item = $opcionExplode[1];
                        $subitem = $opcionExplode[2];
    
                        $menu = new Menuperfil();
                        $menu->usuario_idusuario = $model->id;
                        $menu->opcion = $opcion;
                        $menu->item = $item;
                        $menu->subitem = $subitem;
                        $menu->save();
                    }
                }


                return $this->redirect(['view', 'id' => $model->id]);
            }

            // return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('_form', [
                'model' => $model,
                'menuperfil' => $menuperfil,
                    'useropcion' => $useropcion,
                    'useritem' => $useritem,
                    'usersubitem' => $usersubitem,
                    'opciones' => $opciones,
                    'items' => $items,
                    'subitems' => $subitems,
            ]);
        }
    }

    /**
     * Updates an existing Rol model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        $roles = Rol::find()->all();
        $menuperfil = MenuPerfil::find()->where(['usuario_idusuario' => $id])->all();
        
        $useropcion = array();
        $useritem = array();
        $usersubitem = array();

        if ($menuperfil) {
            foreach ($menuperfil as $m) {
                // OPCION
                if ($m->opcion && !$m->item && !$m->subitem) {
                    array_push($useropcion, $m->opcion);
                }
                // ITEM
                if ($m->opcion && $m->item && !$m->subitem) {
                    array_push($useritem, $m->item);
                }
                // SUBITEM
                if ($m->opcion && $m->item && $m->subitem) {
                    array_push($usersubitem, $m->subitem);
                }
            }
        }

        $opciones = OpcionMenu::find()->all();
        $items = ItemMenu::find()->all();
        $subitems = SubitemMenu::find()->all();


        if ($model->load(Yii::$app->request->post()) ) {

            Menuperfil::deleteAll(['usuario_idusuario' => $id]);
            // CREACION DEL MENU PARA EL PERFIL CREADO
            if (isset($_POST['opciones'])) {
                foreach ($_POST['opciones'] as $opcion) {
                    
                    $opcionExplode = explode(',', $opcion);
                    $opcion = $opcionExplode[0];
                    $item = $opcionExplode[1];
                    $subitem = $opcionExplode[2];

                    $menu = new Menuperfil();
                    $menu->usuario_idusuario = $id;
                    $menu->opcion = $opcion;
                    $menu->item = $item;
                    $menu->subitem = $subitem;
                    $menu->save();
                }
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            // return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'menuperfil' => $menuperfil,
                    'useropcion' => $useropcion,
                    'useritem' => $useritem,
                    'usersubitem' => $usersubitem,
                    'opciones' => $opciones,
                    'items' => $items,
                    'subitems' => $subitems,
            ]);
        }
    }

    /**
     * Deletes an existing Rol model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // echo "<pre>";
        // print_r($_REQUEST);
        // die;
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Rol model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rol the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rol::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
