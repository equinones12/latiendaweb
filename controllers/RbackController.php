<?php

namespace app\controllers;

use Yii;
use app\models\Controladores;
use app\models\Rol;
use app\models\User;
use app\models\Rback;
use app\models\RbackSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * RbackController implements the CRUD actions for Rback model.
 */
class RbackController extends Controller
{

    public function rback(){

        $Rback = array('none');

        $controlador = Controladores::find()->where(['nombreControlador' => Yii::$app->controller->id])->one();

        $arrayRback = Rback::find()
                        ->where(['idRol' => yii::$app->user->identity->role])
                        ->andWhere(['idControlador' => $controlador->idcontrolador])
                        ->andWhere(['estadoRback' => 1])
                        ->all();

        foreach ($arrayRback as $r) {
            array_push($Rback, $r->idAccion0->nombreAccion);
        }

        return $Rback;
    }
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' =>  ['index','create','view'],
                'rules' => [
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => $this->rback(),
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            return User::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => $this->rback(),
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            return User::isUserRecaudo(Yii::$app->user->identity->id);
                        },
                    ],
                ],
            ],
            // la accion delete solo puede ser accedida por metodo POST
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rback models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RbackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $roles = Rol::find()->all();
        $rback = Rback::find()->all();
        $controlador = Controladores::find()->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'roles' => $roles,
            'rback' => $rback,
            'controlador' => $controlador,
        ]);
    }

    /**
     * Displays a single Rback model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Rback model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id,$idRol)
    {
        $model = new Rback();

        $rol = Rol::findOne($idRol);
        $controlador = Controladores::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idrback]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'id' => $id,
                'idRol' => $idRol,
                'rol' => $rol,
                'controlador' => $controlador,
            ]);
        }
    }

    /**
     * Updates an existing Rback model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idrback]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionEstateaccion($id,$estado)
    {
        $model = $this->findModel($id);

        $model->estadoRback = $estado;
        $model->save();
        
    }

    /**
     * Finds the Rback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rback::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
