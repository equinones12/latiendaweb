<?php

namespace app\controllers;

use Yii;
use yii\db\Query;
use app\models\Rol;
use app\models\User;
use app\models\Users;
use yii\web\Controller;
use app\models\ItemMenu;
use app\models\Menuperfil;
use app\models\OpcionMenu;

use app\models\Extensiones;
use app\models\SubitemMenu;
use app\models\UsersSearch;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\UsersDocumentos;
use yii\web\NotFoundHttpException;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','update','delete','view'],
                'rules' => [
                    // Acciones rol ADMINISTRADOR
                    [
                        'actions' => ['index', 'update','view'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $model = Users::find()->joinWith('rol')->all();
        $query = new Query();
        $connection = Yii::$app->db;
        $query = $connection->createCommand('SELECT     *, users.id as usuario
                                                from    users
                                                        join rol on (role = rol.id)
                                                where   1=1
                                            ');
        $model = $query->queryAll();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }


    public function actionInactivar($id){

        $query = new Query();
        $connection = Yii::$app->db;

        // echo "<pre>";
        // print_r($_REQUEST);
        // die;
        
        $query = $connection->createCommand('UPDATE users
                                                SET activate = 0 
                                                where   id = '.$id.''
                                                            )->execute();
        // die;
        return $this->redirect(['index']);
    }

    public function actionActivar($id){

        $query = new Query();
        $connection = Yii::$app->db;

        // echo "<pre>";
        // print_r($_REQUEST);
        // die;
        
        $query = $connection->createCommand('UPDATE users
                                                SET activate = 1 
                                                where   id = '.$id.''
                                                                )->execute();
        // die;
        return $this->redirect(['index']);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   

        $documentos = UsersDocumentos::find()->where(['user_documento_cliente_id' => $id])->all();
        $documentosextension = UsersDocumentos::find()->where(['user_documento_cliente_id' => $id])->groupBy('user_documento_tipo_archivo')->all();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'documentos' => $documentos,
            'documentosextension' => $documentosextension,
            'modelDocumento' => new UsersDocumentos(),
        ]);
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // 
        $roles = Rol::find()->all();
        $menuperfil = MenuPerfil::find()->where(['usuario_idusuario' => $id])->all();
        
        $useropcion = array();
        $useritem = array();
        $usersubitem = array();

        if ($menuperfil) {
            foreach ($menuperfil as $m) {
                // OPCION
                if ($m->opcion && !$m->item && !$m->subitem) {
                    array_push($useropcion, $m->opcion);
                }
                // ITEM
                if ($m->opcion && $m->item && !$m->subitem) {
                    array_push($useritem, $m->item);
                }
                // SUBITEM
                if ($m->opcion && $m->item && $m->subitem) {
                    array_push($usersubitem, $m->subitem);
                }
            }
        }


        $opciones = OpcionMenu::find()->all();
        $items = ItemMenu::find()->all();
        $subitems = SubitemMenu::find()->all();
        // 
        
        if (yii::$app->user->identity->role == 1) {
            $model = $this->findModel($id);
            $actualPassword = $model->password;


            
            if ($model->load(Yii::$app->request->post())) {


                // echo "<pre>";
                // print_r($_REQUEST);
                // die;
                
                // if (isset($_REQUEST['Users']['fechaInicioContrato'])) {
                //     $model->fechaInicioContrato = $_REQUEST['Users']['fechaInicioContrato'];
                    
                // }
                // if (isset($_REQUEST['Users']['fechaFinalContrato'])) {
                //     $model->fechaFinalContrato = $_REQUEST['Users']['fechaFinalContrato'];
                    
                // }

                // if ($_REQUEST['Users']['tipoContrato']=="") {
                //     $model->tipoContrato = 497;
                // }

                // ELIMINAR LOS MENUS ASOCIADOS ANTERIORMENTE
                Menuperfil::deleteAll(['usuario_idusuario' => $id]);
                // CREACION DEL MENU PARA EL PERFIL CREADO
                if (isset($_POST['opciones'])) {
                    foreach ($_POST['opciones'] as $opcion) {
                        
                        $opcionExplode = explode(',', $opcion);
                        $opcion = $opcionExplode[0];
                        $item = $opcionExplode[1];
                        $subitem = $opcionExplode[2];

                        $menu = new Menuperfil();
                        $menu->usuario_idusuario = $id;
                        $menu->opcion = $opcion;
                        $menu->item = $item;
                        $menu->subitem = $subitem;
                        $menu->save();

                    }
                }





                if ($actualPassword != $model->password) {
                    $model->password = crypt($model->password, Yii::$app->params["salt"]);
                }
                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } else {
                
                return $this->render('update', [
                    'model' => $model,
                    'menuperfil' => $menuperfil,
                    'useropcion' => $useropcion,
                    'useritem' => $useritem,
                    'usersubitem' => $usersubitem,
                    'opciones' => $opciones,
                    'items' => $items,
                    'subitems' => $subitems,
                ]);
            }
        }else{



            if (yii::$app->user->identity->id == $id) {
                $model = $this->findModel($id);
                    $actualPassword = $model->password;
                    if ($model->load(Yii::$app->request->post())) {
                        if ($actualPassword != $model->password) {
                            $model->password = crypt($model->password, Yii::$app->params["salt"]);
                        }
                        if ($model->save()) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    } else {
                        return $this->render('update', [
                            'model' => $model,
                            'menuperfil' => $menuperfil,
                            'useropcion' => $useropcion,
                            'useritem' => $useritem,
                            'usersubitem' => $usersubitem,
                            'opciones' => $opciones,
                            'items' => $items,
                            'subitems' => $subitems,
                        ]);
                    }
                }else{
                    $model = $this->findModel(yii::$app->user->identity->id);
                    
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }
            }

        }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUploadfiles()
    {
        // echo "<pre>";
        // print_r($_POST);
        // print_r($_FILES);
        // die;

        $contFile = 0;
        foreach ($_FILES['UsersDocumentos']['name']['imageFiles'] as $files) {

            $documento = new UsersDocumentos();
            $documento->user_documento_cliente_id = $_POST['idOrden'];
            $documento->user_documento_nombreorigina       = $_FILES['UsersDocumentos']['name']['imageFiles'][$contFile];
            $documento->user_documento_tipodocumento       = $_FILES['UsersDocumentos']['type']['imageFiles'][$contFile];
            $documento->user_documento_tipo_archivo        = $_REQUEST['UsersDocumentos']['user_documento_tipo_archivo'];
            $documento->user_documento_fechaupload         = date('Y-m-d H:i:s');

            if ($documento->save() ) {


                $ext = pathinfo($_FILES['UsersDocumentos']['name']['imageFiles'][$contFile], PATHINFO_EXTENSION);
                $nuevonombre = date('Ymd') . '_' . $_POST['idOrden'] . '_' . $documento->user_documento_id . '.' . $ext;
                $pathFile = 'documentos_usuarios/' . $nuevonombre;
                move_uploaded_file($_FILES['UsersDocumentos']['tmp_name']['imageFiles'][$contFile],  $pathFile );

                $extension = Extensiones::find()->where(['extensiones_extension' => strtoupper($ext) ])->one();
                if ($extension) {
                    $extensionid = $extension->idextensiones;
                }else{
                    $extensionid = 1;
                }

                $documentoUpdate = UsersDocumentos::findOne($documento->user_documento_id);
                $documentoUpdate->user_documento_nuevonombre = $nuevonombre;
                $documentoUpdate->user_documento_extension = $extensionid;
                if ($documentoUpdate->update() ) {

                }else{
                    echo '<pre>';
                    echo '$documentoUpdate->update() <br>';
                    print_r($documentoUpdate->getErrors() );
                    die();
                }

            }else{
                echo '<pre>';
                echo '$documento->save <br>';
                print_r($documento->getErrors() );
                die();
            }

            $contFile++;
        }

        return $this->redirect(['view', 'id' => $_POST['idOrden'] ]);
   
    }

    public function actionEliminardocumento($id)
    {
        echo '<pre>';

        $model = UsersDocumentos::findOne($id);
        $nombreArchivo = $model->user_documento_nuevonombre;

        $model->delete();

        unlink(__DIR__ . '/../web/documentos_usuarios/'.$nombreArchivo);

    }

}
