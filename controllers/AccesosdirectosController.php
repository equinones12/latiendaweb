<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Rol;
use app\models\AccesosDirectos;
use app\models\AccesosDirectosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;



/**
 * AccesosdirectosController implements the CRUD actions for AccesosDirectos model.
 */
class AccesosdirectosController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'index', 'view'],
                'rules' => [
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => ['update','index','view'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            return User::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                ],
            ],
            // la accion delete solo puede ser accedida por metodo POST
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AccesosDirectos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $roles = Rol::find()->all();
        $accesosdirectos = AccesosDirectos::find()->all();

        return $this->render('index', [
            'roles' => $roles,
            'accesosdirectos' => $accesosdirectos,
        ]);
    }

    public function actionGuardarcambios()
    {
        AccesosDirectos::deleteAll();
        if (isset($_REQUEST['diagnostico'])) {
            foreach ($_REQUEST['diagnostico'] as $rol) {
                $accesosdirectos = new AccesosDirectos();
                $accesosdirectos->numerotapa = 1;
                $accesosdirectos->idRol = $rol;
                $accesosdirectos->save();
            }
        }
        if (isset($_REQUEST['comercial'])) {
            foreach ($_REQUEST['comercial'] as $rol) {
                $accesosdirectos = new AccesosDirectos();
                $accesosdirectos->numerotapa = 2;
                $accesosdirectos->idRol = $rol;
                $accesosdirectos->save();
            }
        }
        if (isset($_REQUEST['calidad'])) {
            foreach ($_REQUEST['calidad'] as $rol) {
                $accesosdirectos = new AccesosDirectos();
                $accesosdirectos->numerotapa = 3;
                $accesosdirectos->idRol = $rol;
                $accesosdirectos->save();
            }
        }
        if (isset($_REQUEST['programador'])) {
            foreach ($_REQUEST['programador'] as $rol) {
                $accesosdirectos = new AccesosDirectos();
                $accesosdirectos->numerotapa = 4;
                $accesosdirectos->idRol = $rol;
                $accesosdirectos->save();
            }
        }
        if (isset($_REQUEST['almacen'])) {
            foreach ($_REQUEST['almacen'] as $rol) {
                $accesosdirectos = new AccesosDirectos();
                $accesosdirectos->numerotapa = 5;
                $accesosdirectos->idRol = $rol;
                $accesosdirectos->save();
            }
        }
        if (isset($_REQUEST['tecnicos'])) {
            foreach ($_REQUEST['tecnicos'] as $rol) {
                $accesosdirectos = new AccesosDirectos();
                $accesosdirectos->numerotapa = 6;
                $accesosdirectos->idRol = $rol;
                $accesosdirectos->save();
            }
        }
        if (isset($_REQUEST['distribucion'])) {
            foreach ($_REQUEST['distribucion'] as $rol) {
                $accesosdirectos = new AccesosDirectos();
                $accesosdirectos->numerotapa = 7;
                $accesosdirectos->idRol = $rol;
                $accesosdirectos->save();
            }
        }
        if (isset($_REQUEST['compras'])) {
            foreach ($_REQUEST['compras'] as $rol) {
                $accesosdirectos = new AccesosDirectos();
                $accesosdirectos->numerotapa = 8;
                $accesosdirectos->idRol = $rol;
                $accesosdirectos->save();
            }
        }

        if (isset($_REQUEST['caja'])) {
            foreach ($_REQUEST['caja'] as $rol) {
                $accesosdirectos = new AccesosDirectos();
                $accesosdirectos->numerotapa = 9;
                $accesosdirectos->idRol = $rol;
                $accesosdirectos->save();
            }
        }
        if (isset($_REQUEST['trabajosexternos'])) {
            foreach ($_REQUEST['trabajosexternos'] as $rol) {
                $accesosdirectos = new AccesosDirectos();
                $accesosdirectos->numerotapa = 10;
                $accesosdirectos->idRol = $rol;
                $accesosdirectos->save();
            }
        }

        

        return $this->redirect(['site/index']);
        
    }

    

    /**
     * Displays a single AccesosDirectos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AccesosDirectos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AccesosDirectos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idaccesos_directos]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AccesosDirectos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idaccesos_directos]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the AccesosDirectos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AccesosDirectos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AccesosDirectos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
