<?php

namespace app\controllers;

use Yii;
use app\models\OpcionHasRol;
use app\models\OpcionMenu;
use app\models\ItemHasRol;
use app\models\SubitemHasRol;
use app\models\OpcionHasRolSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Rol;
use app\models\User;
use app\models\Rback;
use app\models\Controladores;

/**
 * OpcionhasrolController implements the CRUD actions for OpcionHasRol model.
 */
class OpcionhasrolController extends Controller
{
    public function rback(){

        $Rback = array('none');

        $controlador = Controladores::find()->where(['nombreControlador' => Yii::$app->controller->id])->one();

        $arrayRback = Rback::find()
                        ->where(['idRol' => yii::$app->user->identity->role])
                        ->andWhere(['idControlador' => $controlador->idcontrolador])
                        ->andWhere(['estadoRback' => 1])
                        ->all();

        foreach ($arrayRback as $r) {
            array_push($Rback, $r->idAccion0->nombreAccion);
        }

        return $Rback;
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' =>  ['update','index','view'],
                'rules' => [
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => $this->rback(),
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            return User::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                ],
            ],
            // la accion delete solo puede ser accedida por metodo POST
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OpcionHasRol models.
     * @return mixed
     */
    public function actionIndex()
    {

        // $rol = Rol::find()->where('id <> 1')->all();
        // $opcion = OpcionHasRol::find()->where('id_rol <> 1')->all();
        // $item = ItemHasRol::find()->where('id_rol <> 1')->all();
        // $subitem = SubitemHasRol::find()->where('id_rol <> 1')->all();

        $rol = Rol::find()->all();
        $opcion = OpcionHasRol::find()->all();
        $item = ItemHasRol::find()->all();
        $subitem = SubitemHasRol::find()->all();

        return $this->render('index', [
            'rol' => $rol,
            'opcion' => $opcion,
            'item' => $item,
            'subitem' => $subitem,
        ]);
    }

    /**
     * Displays a single OpcionHasRol model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OpcionHasRol model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        
        $modelRol = new OpcionHasRol();

        if ($modelRol->load(Yii::$app->request->post())) {

            if ($modelRol->save()) {
                return $this->redirect(['index']);
            }else{
                echo 'error 2';
            }

        } else {
            return $this->render('create', [
                'model' => $modelRol,
            ]);
        }
    }

    public function actionOpcion($id,$estado){
        $model = $this->findModel($id);
        $model->estado = $estado;
        $model->update();
    }

    /**
     * Updates an existing OpcionHasRol model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);



        if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the OpcionHasRol model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OpcionHasRol the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OpcionHasRol::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
