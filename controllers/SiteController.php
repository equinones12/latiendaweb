<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\Response;
use app\models\FormRegister; 
use app\models\Users; 
use app\models\AccesosDirectos; 
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Rol;
use yii\db\Query;
use yii\data\Pagination;
use app\models\FormSearch;
use app\models\HistoriasClinicas;
use app\models\OpcionMenu;
use app\models\ItemMenu;
use app\models\SubitemMenu;
use app\models\Menuperfil;
use app\models\Pacientes;
use yii\helpers\Url;


use yii\helpers\Html;


class SiteController extends Controller
{
    // public function behaviors()
    // {
    //     return [
    //         //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
    //         //sólo se puede acceder a través del método post
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'logout' => ['post','get'],
    //             ],
    //         ],
    //     ];
    // }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    

    public function actionIndex()
    {   

        if (!Yii::$app->user->isGuest) {
            # code...
        

            $userIdentity = Users::findOne(yii::$app->user->identity->id);
            // CONSULTAS ACCESOS DIRECTOS
                $query = new Query();
                $connection = Yii::$app->db;
                $accesosDirectos = array();
                $arrayTapas = AccesosDirectos::find()
                                    ->where(['idRol' => yii::$app->user->identity->role ])
                                    ->all();
                foreach ($arrayTapas as $a) {
                    array_push($accesosDirectos, $a->numerotapa);
                }
                $roles = Rol::find()->all();
                
            $idUsuario =  Yii::$app->user->identity->id;

            // se instancia la Clase Query.
            $query = new Query();
            $connection = Yii::$app->db;

            $query = $connection->createCommand('SELECT count(*) as total
                                                from    peliculas
                                               ');
            $peliculas = $query->queryOne();                       


            // echo "<pre>";
            // print_r($estadistica2);
            // echo "<br>";
            // print_r($estadisticanum2);
            // die;


            return $this->render('index',[
                'userIdentity' => $userIdentity,
                'accesosDirectos' => $accesosDirectos,
                'peliculas' => $peliculas,               

            ]);


        }else{

            return $this->redirect(["site/login"]);
            
        }

    }



    
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(["site/index"]);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

             return $this->redirect(["site/index"]);

        } else {
             return $this->render('login', [
                 'model' => $model,
             ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionUser(){
        return $this->render("user");
    }

    public function actionAdmin(){
        return $this->render("admin");
    }

    private function randKey($str='', $long=0){
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;

        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }

        return $key;
    }

    public function actionConfirm(){

        $table = new Users;

        if (Yii::$app->request->get())
        {
            //Obtenemos el valor de los parámetros get
            $id = Html::encode($_GET["id"]);
            $authKey = $_GET["authKey"];
        
            if ((int) $id)
            {
                //Realizamos la consulta para obtener el registro
                $model = $table
                ->find()
                ->where("id=:id", [":id" => $id])
                ->andWhere("authKey=:authKey", [":authKey" => $authKey]);
      
                //Si el registro existe
                if ($model->count() == 1)
                {
                    $activar = Users::findOne($id);
                    $activar->activate = 1;

                    if ($activar->update())
                    {
                        echo "Enhorabuena registro llevado a cabo correctamente, redireccionando ...";
                        echo "<meta http-equiv='refresh' content='8; ".Url::toRoute("site/login")."'>";
                    }
                    else
                    {
                        echo "Ha ocurrido un error al realizar el registro, redireccionando ...";
                        echo "<meta http-equiv='refresh' content='8; ".Url::toRoute("site/login")."'>";

                    }
                }
                else //Si no existe redireccionamos a login
                {
                    return $this->redirect(["site/login"]);
                }
            }
            else //Si id no es un número entero redireccionamos a login
            {
                return $this->redirect(["site/login"]);
            }
        }
    }

    public function actionRegister()
    { 

        // 


        $opciones = OpcionMenu::find()->all();
        $items = ItemMenu::find()->all();
        $subitems = SubitemMenu::find()->all();
        // 
        
        //Creamos la instancia con el model de validación
        $model = new FormRegister;

        //Mostrará un mensaje en la vista cuando el usuario se haya registrado
        $msg = null;

        //Validación mediante ajax
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);

        }

        if ($model->load(Yii::$app->request->post()))
        {
            
            if($model->validate())
            {

                // echo "<pre>";
                // print_r($_REQUEST);
                // die();

                //Preparamos la consulta para guardar el usuario
                $user = new Users;
                $user->username = $model->username;
                $user->email = $model->email;
                $user->role = $model->role; 
                $user->nombres = $model->nombres; 
                $user->apellidos = $model->apellidos; 
                $user->numeroIdentificacion = $model->numeroIdentificacion; 
                $user->direccion = $model->direccion; 
                $user->telefono = $model->telefono; 
                
                //Encriptamos el password
                $user->password = md5($model->password);                // $user->password = md5($model->password);
                //Creamos una cookie para autenticar al usuario cuando decida recordar la sesión, esta misma
                //clave será utilizada para activar el usuario
                $user->authKey = $this->randKey("abcdef0123456789", 200);
                //Creamos un token de acceso único para el usuario
                $user->accessToken = $this->randKey("abcdef0123456789", 200);
                //Si el registro es guardado correctamente
                if ($user->insert())
                {

                    // CREACION DEL MENU PARA EL PERFIL CREADO
                    if (isset($_POST['opciones'])) {
                        foreach ($_POST['opciones'] as $opcion) {
                            
                            $opcionExplode = explode(',', $opcion);
                            $opcion = $opcionExplode[0];
                            $item = $opcionExplode[1];
                            $subitem = $opcionExplode[2];
                            $menu = new Menuperfil();
                            $menu->usuario_idusuario = $user->id;
                            $menu->opcion = $opcion;
                            $menu->item = $item;
                            $menu->subitem = $subitem;
                            $menu->save();
                        }
                    }
                    
                    $newUser = Users::findOne($user->id);
                    $msg = "El funcionario ".$newUser->username.' ha sido registrado';
                    $alert = 'success';
                    $graphicon = 'ok';
                    return $this->render("newUser", 
                        [
                            "model" => $newUser, 
                            "msg" => $msg,
                            "alert" => $alert,
                            "graphicon" => $graphicon
                        ]
                    );
                }
                else
                {
                    print_r($user->getErrors());
                    die;
                    $newUser = false;
                    $msg = "Ha ocurrido un error al llevar a cabo tu registro";
                    $alert = 'danger';
                    $graphicon = 'remove';

                    
                }
            }
            else
            {
                $model->getErrors();

            }
        }
        $roles = Rol::find()->all();
        return $this->render("register", 
            [
                "model" => $model, 
                "msg" => $msg,
                "roles"=>$roles,
                'opciones' => $opciones,
                'items' => $items,
                'subitems' => $subitems,
            ]);
    }
}