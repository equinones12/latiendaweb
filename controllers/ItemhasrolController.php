<?php

namespace app\controllers;

use Yii;
use app\models\ItemHasRol;
use app\models\ItemMenu;
use app\models\Rol;
use app\models\ItemHasRolSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ItemhasrolController implements the CRUD actions for ItemHasRol model.
 */
class ItemhasrolController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ItemHasRol models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ItemHasRolSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionItem($id,$estado){
        $model = $this->findModel($id);
        $model->estado = $estado;
        $model->update();
    }

    /**
     * Displays a single ItemHasRol model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ItemHasRol model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($idRol,$idOpcion)
    {
        $model = new ItemHasRol();
        $rol = Rol::findOne($idRol);

        if ($model->load(Yii::$app->request->post())) {


                    if ($model->save()) {
                        return $this->redirect(['opcionhasrol/index']);
                    }else{
                        echo 'falied save item has rol';
                    }

        } else {
            return $this->render('create', [
                'model' => $model,
                'rol' => $rol,
                'opcion' => $idOpcion,
            ]);
        }
    }

    /**
     * Updates an existing ItemHasRol model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($idRol,$idItem)
    {
        $rol = Rol::findOne($idRol);
        $item = ItemMenu::findOne($idItem);
        $opcion = $item->id_opcion;

        $model = $this->findModel($idItem);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

                return $this->redirect(['opcionhasrol/index']);

        } else {
            return $this->render('update', [
                'model' => $model,
                'rol' => $rol,
                'opcion' => $opcion,
            ]);
        }
    }

    /**
     * Deletes an existing ItemHasRol model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ItemHasRol model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ItemHasRol the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ItemHasRol::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
