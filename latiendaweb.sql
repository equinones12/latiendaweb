-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: latiendaweb
-- ------------------------------------------------------
-- Server version	5.7.36

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accesos_directos`
--

DROP TABLE IF EXISTS `accesos_directos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `accesos_directos` (
  `idaccesos_directos` int(11) NOT NULL AUTO_INCREMENT,
  `numerotapa` int(11) DEFAULT NULL,
  `idRol` int(11) DEFAULT NULL,
  PRIMARY KEY (`idaccesos_directos`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accesos_directos`
--

LOCK TABLES `accesos_directos` WRITE;
/*!40000 ALTER TABLE `accesos_directos` DISABLE KEYS */;
INSERT INTO `accesos_directos` VALUES (14,5,1),(15,5,5),(16,8,1),(17,8,16);
/*!40000 ALTER TABLE `accesos_directos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acciones`
--

DROP TABLE IF EXISTS `acciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `acciones` (
  `idaccion` int(11) NOT NULL AUTO_INCREMENT,
  `nombreAccion` varchar(45) NOT NULL,
  PRIMARY KEY (`idaccion`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acciones`
--

LOCK TABLES `acciones` WRITE;
/*!40000 ALTER TABLE `acciones` DISABLE KEYS */;
INSERT INTO `acciones` VALUES (1,'create'),(2,'view'),(3,'update'),(4,'index'),(5,'logout');
/*!40000 ALTER TABLE `acciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autores`
--

DROP TABLE IF EXISTS `autores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `autores` (
  `id_autor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(120) NOT NULL,
  PRIMARY KEY (`id_autor`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autores`
--

LOCK TABLES `autores` WRITE;
/*!40000 ALTER TABLE `autores` DISABLE KEYS */;
INSERT INTO `autores` VALUES (1,'Marlon Brando'),(2,'Clint Eastwood'),(3,'Charles Chaplin'),(4,'Tom Hanks'),(5,'Harrison Ford'),(6,'Denzel Washington'),(7,'Steven Spielberg'),(8,'Peter Jackson'),(9,'Michael Bay'),(10,'James Cameron'),(11,'Chris Columbus'),(12,'Tim Burton');
/*!40000 ALTER TABLE `autores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categorias` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(120) NOT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias`
--

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` VALUES (1,'TERROR'),(2,'CIENCIA FICCIÓN'),(3,'HUMOR'),(4,'COMEDIA'),(5,'CINE ADULTOS'),(6,'ACCIÓN');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controladores`
--

DROP TABLE IF EXISTS `controladores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `controladores` (
  `idcontrolador` int(11) NOT NULL,
  `nombreControlador` varchar(45) NOT NULL,
  PRIMARY KEY (`idcontrolador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controladores`
--

LOCK TABLES `controladores` WRITE;
/*!40000 ALTER TABLE `controladores` DISABLE KEYS */;
INSERT INTO `controladores` VALUES (1,'funcionarios'),(2,'opcionhasrol'),(3,'rback'),(4,'site');
/*!40000 ALTER TABLE `controladores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_has_rol`
--

DROP TABLE IF EXISTS `item_has_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_has_rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_item` int(11) DEFAULT NULL,
  `id_rol` int(11) DEFAULT NULL,
  `estado` int(1) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_has_rol`
--

LOCK TABLES `item_has_rol` WRITE;
/*!40000 ALTER TABLE `item_has_rol` DISABLE KEYS */;
INSERT INTO `item_has_rol` VALUES (46,1,1,1,4),(47,2,1,1,6),(48,3,1,1,1),(49,4,1,1,9);
/*!40000 ALTER TABLE `item_has_rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_menu`
--

DROP TABLE IF EXISTS `item_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(45) DEFAULT NULL,
  `etiqueta` varchar(45) DEFAULT NULL,
  `id_opcion` int(11) DEFAULT NULL,
  `activo` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_menu`
--

LOCK TABLES `item_menu` WRITE;
/*!40000 ALTER TABLE `item_menu` DISABLE KEYS */;
INSERT INTO `item_menu` VALUES (1,'peliculas/index','Ver peliculas',2,1),(2,'peliculas/create','Crear pelicula',2,1),(3,'pelicula/update','Actualizar pelicula',2,1);
/*!40000 ALTER TABLE `item_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_perfil`
--

DROP TABLE IF EXISTS `menu_perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu_perfil` (
  `idmenu_perfil` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_idusuario` int(11) DEFAULT NULL,
  `opcion` int(11) DEFAULT NULL,
  `item` int(11) DEFAULT NULL,
  `subitem` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmenu_perfil`)
) ENGINE=MyISAM AUTO_INCREMENT=40561 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_perfil`
--

LOCK TABLES `menu_perfil` WRITE;
/*!40000 ALTER TABLE `menu_perfil` DISABLE KEYS */;
INSERT INTO `menu_perfil` VALUES (1,1,2,1,0),(40560,1,2,2,NULL),(40559,1,2,NULL,NULL),(2,1,2,NULL,NULL);
/*!40000 ALTER TABLE `menu_perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1666842229),('m221027_033216_migracion',1666842241),('m221027_035936_migracion2',1666843219);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opcion_has_rol`
--

DROP TABLE IF EXISTS `opcion_has_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `opcion_has_rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_opcion` int(11) DEFAULT NULL,
  `id_rol` int(11) DEFAULT NULL,
  `estado` int(1) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opcion_has_rol`
--

LOCK TABLES `opcion_has_rol` WRITE;
/*!40000 ALTER TABLE `opcion_has_rol` DISABLE KEYS */;
INSERT INTO `opcion_has_rol` VALUES (1,1,1,1,3),(2,2,1,1,1),(3,3,1,1,2),(22,7,1,1,6);
/*!40000 ALTER TABLE `opcion_has_rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opcion_menu`
--

DROP TABLE IF EXISTS `opcion_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `opcion_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `estado` int(1) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `icono` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opcion_menu`
--

LOCK TABLES `opcion_menu` WRITE;
/*!40000 ALTER TABLE `opcion_menu` DISABLE KEYS */;
INSERT INTO `opcion_menu` VALUES (1,'ConfiguraciÃ³n',1,1,'wrench'),(2,'peliculas',1,3,'list');
/*!40000 ALTER TABLE `opcion_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `peliculas`
--

DROP TABLE IF EXISTS `peliculas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `peliculas` (
  `id_pelicula` int(11) NOT NULL AUTO_INCREMENT,
  `id_categoria` int(11) DEFAULT NULL,
  `id_autor` int(11) DEFAULT NULL,
  `nombre` varchar(120) DEFAULT NULL,
  `fecha_lanzamiento` date DEFAULT NULL,
  `productora` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`id_pelicula`),
  KEY `fk_categoria` (`id_categoria`),
  KEY `fk_autor` (`id_autor`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `peliculas`
--

LOCK TABLES `peliculas` WRITE;
/*!40000 ALTER TABLE `peliculas` DISABLE KEYS */;
INSERT INTO `peliculas` VALUES (2,1,1,'La familia','2022-10-28','Sony'),(3,1,11,'Prueba','2022-10-28','Desconocida');
/*!40000 ALTER TABLE `peliculas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rback`
--

DROP TABLE IF EXISTS `rback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rback` (
  `idrback` int(11) NOT NULL AUTO_INCREMENT,
  `idRol` int(11) NOT NULL,
  `idControlador` int(11) NOT NULL,
  `idAccion` int(11) NOT NULL,
  `estadoRback` int(1) NOT NULL,
  PRIMARY KEY (`idrback`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rback`
--

LOCK TABLES `rback` WRITE;
/*!40000 ALTER TABLE `rback` DISABLE KEYS */;
INSERT INTO `rback` VALUES (1,1,1,2,1),(2,1,1,3,1),(3,1,1,4,1),(4,1,2,4,1),(5,1,2,3,1),(6,1,3,4,1),(7,1,3,1,1),(8,1,3,2,1),(9,1,2,2,1);
/*!40000 ALTER TABLE `rback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (1,'ADMINISTRADOR'),(2,'VENDEDOR'),(3,'ESPECIAL');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subitem_has_rol`
--

DROP TABLE IF EXISTS `subitem_has_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subitem_has_rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_subitem` int(11) DEFAULT NULL,
  `id_rol` int(11) DEFAULT NULL,
  `estado` int(1) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subitem_has_rol`
--

LOCK TABLES `subitem_has_rol` WRITE;
/*!40000 ALTER TABLE `subitem_has_rol` DISABLE KEYS */;
/*!40000 ALTER TABLE `subitem_has_rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subitem_menu`
--

DROP TABLE IF EXISTS `subitem_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subitem_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(45) DEFAULT NULL,
  `etiqueta` varchar(45) DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subitem_menu`
--

LOCK TABLES `subitem_menu` WRITE;
/*!40000 ALTER TABLE `subitem_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `subitem_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_identificacion`
--

DROP TABLE IF EXISTS `tipo_identificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_identificacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_identificacion`
--

LOCK TABLES `tipo_identificacion` WRITE;
/*!40000 ALTER TABLE `tipo_identificacion` DISABLE KEYS */;
INSERT INTO `tipo_identificacion` VALUES (1,'Cedula de Ciudadania'),(2,'Nit'),(3,'Cedula de Extranjeria'),(4,'Tarjeta de Identidad');
/*!40000 ALTER TABLE `tipo_identificacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos`
--

DROP TABLE IF EXISTS `tipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipos` (
  `tipo_id` int(5) NOT NULL AUTO_INCREMENT,
  `tipo_nombre` char(255) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_activo` tinyint(1) DEFAULT '1',
  `tipo_creador` int(80) DEFAULT NULL,
  `tipo_fcreador` date DEFAULT NULL,
  `tipo_modificado` int(80) DEFAULT NULL,
  `tipo_fmodificado` date DEFAULT NULL,
  PRIMARY KEY (`tipo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipos`
--

LOCK TABLES `tipos` WRITE;
/*!40000 ALTER TABLE `tipos` DISABLE KEYS */;
INSERT INTO `tipos` VALUES (1,'TIPO DOCUMENTO',1,1,'2015-04-10',0,'0000-00-00'),(2,'SALIDAS',1,1,NULL,NULL,NULL),(3,'ESTADOS VENTAS',1,1,NULL,NULL,NULL),(4,'TARJETAS',1,1,NULL,NULL,NULL),(5,'SODEXO',1,1,NULL,NULL,NULL),(6,'BIG PASS',1,1,NULL,NULL,NULL),(7,'QPASS',1,1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos_detalles`
--

DROP TABLE IF EXISTS `tipos_detalles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipos_detalles` (
  `tipo_detalle_id` int(5) NOT NULL AUTO_INCREMENT,
  `tipo_detalle_tipo_id` int(5) NOT NULL,
  `tipo_detalle_nombre` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipo_detalle_activo` tinyint(1) DEFAULT '1',
  `tipo_detalle_creador` int(80) DEFAULT NULL,
  `tipo_detalle_fcreador` date DEFAULT NULL,
  `tipo_detalle_modificado` int(80) DEFAULT NULL,
  `tipo_detalle_fmodificado` date DEFAULT NULL,
  PRIMARY KEY (`tipo_detalle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipos_detalles`
--

LOCK TABLES `tipos_detalles` WRITE;
/*!40000 ALTER TABLE `tipos_detalles` DISABLE KEYS */;
INSERT INTO `tipos_detalles` VALUES (1,1,'CEDULA',1,1,'0000-00-00',0,'0000-00-00'),(2,1,'CEDULA EXTRANJERIA',1,1,'0000-00-00',0,'0000-00-00'),(3,1,'NIT',1,1,'0000-00-00',0,'0000-00-00'),(4,2,'VENTA',1,1,NULL,NULL,NULL),(5,2,'DAÃO',1,1,NULL,NULL,NULL),(6,2,'BAJA',1,1,NULL,NULL,NULL),(7,2,'ROBO',1,1,NULL,NULL,NULL),(8,3,'ACTIVA',1,1,NULL,NULL,NULL),(9,3,'ANULADA',1,1,NULL,NULL,NULL),(10,4,'CREDITO',1,1,NULL,NULL,NULL),(11,4,'DEBITO',1,1,NULL,NULL,NULL),(12,4,'NEQUI',1,1,NULL,NULL,NULL),(13,4,'MOVI',1,1,NULL,NULL,NULL),(14,4,'DAVIPLATA',1,1,NULL,NULL,NULL),(15,5,'RESTAURANTE',1,1,NULL,NULL,NULL),(16,5,'CANASTA',1,1,NULL,NULL,NULL),(17,5,'PREMIUM',1,1,NULL,NULL,NULL),(18,6,'ALIMENTICIO',1,1,NULL,NULL,NULL),(19,6,'CANASTA',1,1,NULL,NULL,NULL),(20,6,'REGALO',1,1,NULL,NULL,NULL),(21,7,'ALIMENTICIO',1,1,NULL,NULL,NULL),(22,7,'CANASTA',1,1,NULL,NULL,NULL),(23,7,'REGALO',1,1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tipos_detalles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(250) NOT NULL,
  `authKey` varchar(250) NOT NULL,
  `accessToken` varchar(250) NOT NULL,
  `activate` tinyint(1) NOT NULL DEFAULT '1',
  `role` varchar(45) DEFAULT NULL,
  `nombres` varchar(60) DEFAULT NULL,
  `apellidos` varchar(60) DEFAULT NULL,
  `foto` varchar(250) DEFAULT 'default.png',
  `numeroIdentificacion` double DEFAULT NULL,
  `direccion` varchar(150) DEFAULT NULL,
  `telefono` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','administrador@gmail.com','d1ba957ebc4998838e4a880aef3183d3','6faad25298cfc2c00dac6c437bfd5d98be8d818b720a84c8733e312b8ce656405bce9d12aa92b42f462e7551eb2ead65556d08f7409932f3787c16f5b9d362d8be6bc04a44ee2e15082816f7e4d775aa75c6f7fb97a06896053273566d49f6e99a8f8b0a','57b1008451ab3f145408c5762ca67e76753f16c00b715820aa36f2620d2bce31e10c7a6b13a5d0aa4db29dbb4c0e7d6acc474c638837b5825eab8672e94f89f104df6cd95c27f9c8cde3ed88e3eef4e69ad6fa9e9e759a6c8a9892fb4cebcc399e3ad6b0',1,'1','Administrador','Sistema','default.png',0,'calle 123',0),(6,'vendedor','vendedor@gmail.com','d1ba957ebc4998838e4a880aef3183d3','95624a6528eeb9f3562b4dd415a718b98a76e2b3c4d4d86ef1c23640e1b79dfe39950bd96f414432a59594748395ef2453fb961a6a7d47d20db7ffd83e24b489e305d77eef73f0ee9a40d5e36d6e36f08207a63983de89d2c848465dd4d509bd54d12e6a','ff46126af85836810f2e56c264842fc4c43e971d19af9de4689ba0cd7c365276447cd2e6df44ad52db4dc9083a56880bcf12a4557d18f52484574ab3d388321d67c3fd1a0e05ecaf76a46536e920a090fa791ba4c45a5b336ac1376b44bb032b124bc2bd',1,'2','vendedor','1','default.png',999999,'calle 123',1231231231);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-27  6:23:12
