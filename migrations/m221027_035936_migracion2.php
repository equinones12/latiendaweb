<?php

use yii\db\Migration;

/**
 * Class m221027_035936_migracion2
 */
class m221027_035936_migracion2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('autores',[
            'id_autor' => $this->primaryKey(),
            'nombre' => $this->string(120)->notNull()
        ]
        
        );

        $this->createTable('peliculas',[
            'id_pelicula' =>$this->primaryKey(),
            'id_categoria' => $this->integer(),
            'id_autor' => $this->integer(),
            'nombre' => $this->string(120),
            'fecha_lanzamiento' => $this->date(),
            'productora' =>$this->string(120)
        ]);

        $this->createTable('categorias',[
            'id_categoria' => $this->primaryKey(),
            'nombre' => $this->string(120)->notNull()
        ]
        
        );

        $this->addForeignKey('fk_categoria',
                             'peliculas','id_categoria','categorias','id_categoria');

        $this->addForeignKey('fk_autor',
                             'peliculas','id_autor','autor','id_autor');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m221027_035936_migracion2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221027_035936_migracion2 cannot be reverted.\n";

        return false;
    }
    */
}
