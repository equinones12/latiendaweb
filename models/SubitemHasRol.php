<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subitem_has_rol".
 *
 * @property integer $id
 * @property integer $id_subitem
 * @property integer $id_rol
 * @property integer $estado
 * @property integer $orden
 *
 * @property SubitemMenu $idSubitem
 * @property Rol $idRol
 */
class SubitemHasRol extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subitem_has_rol';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_subitem', 'id_rol', 'estado', 'orden'], 'integer'],
            [['id_subitem'], 'exist', 'skipOnError' => true, 'targetClass' => SubitemMenu::className(), 'targetAttribute' => ['id_subitem' => 'id']],
            [['id_rol'], 'exist', 'skipOnError' => true, 'targetClass' => Rol::className(), 'targetAttribute' => ['id_rol' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_subitem' => 'Id Subitem',
            'id_rol' => 'Id Rol',
            'estado' => 'Estado',
            'orden' => 'Orden',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSubitem()
    {
        return $this->hasOne(SubitemMenu::className(), ['id' => 'id_subitem']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRol()
    {
        return $this->hasOne(Rol::className(), ['id' => 'id_rol']);
    }
}
