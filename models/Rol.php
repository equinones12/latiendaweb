<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rol".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property ItemHasRol[] $itemHasRols
 * @property OpcionHasRol[] $opcionHasRols
 * @property Rback[] $rbacks
 * @property SubitemHasRol[] $subitemHasRols
 */
class Rol extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rol';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemHasRols()
    {
        return $this->hasMany(ItemHasRol::className(), ['id_rol' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpcionHasRols()
    {
        return $this->hasMany(OpcionHasRol::className(), ['id_rol' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRbacks()
    {
        return $this->hasMany(Rback::className(), ['idRol' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubitemHasRols()
    {
        return $this->hasMany(SubitemHasRol::className(), ['id_rol' => 'id']);
    }
}
