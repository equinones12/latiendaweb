<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AccesosDirectos;

/**
 * AccesosDirectosSearch represents the model behind the search form about `app\models\AccesosDirectos`.
 */
class AccesosDirectosSearch extends AccesosDirectos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idaccesos_directos', 'idRol', 'estado'], 'integer'],
            [['etiqueta', 'action', 'color'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AccesosDirectos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idaccesos_directos' => $this->idaccesos_directos,
            'idRol' => $this->idRol,
            'estado' => $this->estado,
        ]);

        $query->andFilterWhere(['like', 'etiqueta', $this->etiqueta])
            ->andFilterWhere(['like', 'action', $this->action])
            ->andFilterWhere(['like', 'color', $this->color]);

        return $dataProvider;
    }
}
