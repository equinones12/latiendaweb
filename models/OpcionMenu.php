<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "opcion_menu".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $estado
 * @property integer $orden
 *
 * @property ItemMenu[] $itemMenus
 * @property OpcionHasRol[] $opcionHasRols
 */
class OpcionMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'opcion_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estado', 'orden'], 'integer'],
            [['nombre'], 'string', 'max' => 95],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'estado' => 'Estado',
            'orden' => 'Orden',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemMenus()
    {
        return $this->hasMany(ItemMenu::className(), ['id_opcion' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpcionHasRols()
    {
        return $this->hasMany(OpcionHasRol::className(), ['id_opcion' => 'id']);
    }
}
