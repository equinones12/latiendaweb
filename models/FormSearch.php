<?php
namespace app\models;
use Yii;
use yii\base\model;

class FormSearch extends model{
    
    public $orden;
    public $cotizacion;
    public $fecha;
    public $nombre;
    public $valor;
    public $unidad;
    
    public function rules()
    {
        return [
            ["orden", "match", "pattern" => "/^[0-9a-záéíóúñ\s]+$/i", "message" => "Sólo se aceptan letras y números"],
            ["cotizacion", "match", "pattern" => "/^[0-9a-záéíóúñ#$%&\D]+$/i", "message" => "Sólo se aceptan letras y números"],
            ["fecha", "match", "pattern" => "/^[0-9a-záéíóúñ\s]+$/i", "message" => "Sólo se aceptan letras y números"],
            ["nombre", "match", "pattern" => "/^[0-9a-záéíóúñ#$%&\D]+$/", "message" => "Sólo se aceptan letras y números"],
            ["valor", "match", "pattern" => "/^[0-9a-záéíóúñ\s]+$/i", "message" => "Sólo se aceptan letras y números"],
            ["unidad", "match", "pattern" => "/^[0-9a-záéíóúñ#$%&\D]+$/i", "message" => "Sólo se aceptan letras y números"],

        ];
    }
    
    public function attributeLabels()
    {
        return [
            'orden' => "Orden de Servicio:",
            'cotizacion' => "Cotización:",
            'fecha' => "Fecha de Aprobación:",
            'nombre' => "Nombre del Producto:",
            'valor' => "Valor Unidad:",
            'unidad' => "Unidad de Medida:",
        ];
    }
}