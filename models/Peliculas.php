<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "peliculas".
 *
 * @property int $id_pelicula
 * @property int|null $id_categoria
 * @property int|null $id_autor
 * @property string|null $nombre
 * @property string|null $fecha_lanzamiento
 * @property string|null $productora
 */
class Peliculas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'peliculas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_categoria', 'id_autor'], 'integer'],
            [['fecha_lanzamiento'], 'safe'],
            [['nombre', 'productora'], 'string', 'max' => 120],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pelicula' => 'Id Pelicula',
            'id_categoria' => 'Categoria',
            'id_autor' => 'Autor',
            'nombre' => 'Nombre',
            'fecha_lanzamiento' => 'Fecha Lanzamiento',
            'productora' => 'Productora',
        ];
    }

    public function getCategoria(){

        return $this->hasOne(Categorias::className(), ['id_categoria' => 'id_categoria']);
    }
    public function getAutor(){

        return $this->hasOne(Autores::className(), ['id_autor' => 'id_autor']);
    }
}
