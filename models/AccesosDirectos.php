<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "accesos_directos".
 *
 * @property int $idaccesos_directos
 * @property int $numerotapa
 * @property int $idRol
 */
class AccesosDirectos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'accesos_directos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numerotapa', 'idRol'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idaccesos_directos' => 'Idaccesos Directos',
            'numerotapa' => 'Numerotapa',
            'idRol' => 'Id Rol',
        ];
    }
}
