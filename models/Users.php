<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $authKey
 * @property string $accessToken
 * @property integer $activate
 * @property integer $role
 * @property integer $cargo_id
 * @property string $nombres
 * @property string $apellidos
 * @property string $foto
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombres', 'apellidos', 'numeroIdentificacion' ,'username','role' ,'password'], 'required'],
            [['activate','numeroIdentificacion'], 'integer'],
            [['foto','role'], 'string'],
            [['username'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 80],
            [['email'], 'email'],
            [['password', 'authKey', 'accessToken'], 'string', 'max' => 250],
            [['direccion'], 'string', 'max' => 150],
            // ['password', 'match', 'pattern' => "/^.{6,20}$/", 'message' => 'Mínimo 6 y máximo 16 caracteres'],
            [['nombres', 'apellidos'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'activate' => 'Activate',
            'role' => 'Role',
            'nombres' => 'Nombres',
            'apellidos' => 'Apellidos',
            'foto' => 'Foto',
        ];
    }

    public function getRol()
    {
        return $this->hasOne(Rol::className(), ['id' => 'role']);
    }

    

    

    
    

    public function email_existe($attribute, $params)
    {
    
    //Buscar el email en la tabla
    $table = Users::find()->where("email=:email", [":email" => $this->email]);
    
    //Si el email existe mostrar el error
    if ($table->count() == 1)
    {
      $this->addError($attribute, "El email seleccionado existe");
    }
      }
    
      public function username_existe($attribute, $params)
      {
    //Buscar el username en la tabla
    $table = Users::find()->where("username=:username", [":username" => $this->username]);
    
    //Si el username existe mostrar el error
    if ($table->count() == 1)
    {
                  $this->addError($attribute, "El usuario seleccionado existe");
    }
      }
    
}
