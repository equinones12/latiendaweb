<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_has_rol".
 *
 * @property integer $id
 * @property integer $id_item
 * @property integer $id_rol
 * @property integer $estado
 * @property integer $orden
 *
 * @property Rol $idRol
 * @property ItemMenu $idItem
 */
class ItemHasRol extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_has_rol';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_item', 'id_rol', 'estado', 'orden'], 'integer'],
            [['estado', 'orden'], 'required'],
            [['id_rol'], 'exist', 'skipOnError' => true, 'targetClass' => Rol::className(), 'targetAttribute' => ['id_rol' => 'id']],
            [['id_item'], 'exist', 'skipOnError' => true, 'targetClass' => ItemMenu::className(), 'targetAttribute' => ['id_item' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_item' => 'Id Item',
            'id_rol' => 'Id Rol',
            'estado' => 'Estado',
            'orden' => 'Orden',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRol()
    {
        return $this->hasOne(Rol::className(), ['id' => 'id_rol']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdItem()
    {
        return $this->hasOne(ItemMenu::className(), ['id' => 'id_item']);
    }
}
