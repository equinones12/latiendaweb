<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "acciones".
 *
 * @property integer $idaccion
 * @property string $nombreAccion
 *
 * @property Rback[] $rbacks
 */
class Acciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'acciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombreAccion'], 'required'],
            [['nombreAccion'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idaccion' => 'Idaccion',
            'nombreAccion' => 'Nombre Accion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRbacks()
    {
        return $this->hasMany(Rback::className(), ['idAccion' => 'idaccion']);
    }
}
