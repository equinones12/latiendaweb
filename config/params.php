<?php

return [
    'adminEmail' => 'info@cef-aplicaciones.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'bsVersion' => '4.x',
    'title' => 'Sistema de Control',
    'salt' => 'Jistiajti456IIISF',
];
