function tomarImagenPorSeccion(div,nombre,paciente) {

	html2canvas(document.querySelector("#" + div)).then(canvas => {

		var img = canvas.toDataURL();
		console.log(img);
		base = "img=" + img + "&nombre=" + nombre;

		$.ajax({
			type:"POST",
			url:"procesos/crearImagenes.php",
			data:base,
			success:function(respuesta) {	
				respuesta = parseInt(respuesta);
				if (respuesta > 0) {
					// alert("Imagen creada con exito!");
					window.location = 'http://localhost:8080/odontologia/web/index.php?r=pacientes%2Fview&paciente_id=' + paciente;
				} else {
					alert("No se pudo crear la imagen :(");
				}
			}
		});
	});	
}